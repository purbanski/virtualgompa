//
//  LineGraphViewController.h
//  Budda Friend
//
//  Created by Przemek Urbanski on 02/12/14.
//  Copyright (c) 2014 Przemek Urbanski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"

@interface LineGraphViewController : UIViewController<CPTPlotDataSource>

@property (nonatomic, strong) CPTGraphHostingView *hostView;

@end
