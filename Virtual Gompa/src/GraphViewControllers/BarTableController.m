//
//  BarTableController.m
//  Budda Friend
//
//  Created by Przemek Urbanski on 31/12/14.
//  Copyright (c) 2014 Przemek Urbanski. All rights reserved.
//

#import "BarTableController.h"
#import "BarTableCell.h"
#import "ToolBox.h"
#import "SimpleTableCell.h"
#import "StorageManager.h"

@interface BarTableController ()

@property NSArray* records;
@property NSMutableDictionary* dicRecords;
@property NSArray* rawRecords;
@property NSNumber* maxTimeDone;

@end

@implementation BarTableController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tblMeditate.delegate = self;
    self.tblMeditate.dataSource = self;
    
    self.dicRecords = [[NSMutableDictionary alloc]init];
//    [self reload];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
//     self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector:@selector(deviceOrientationDidChange:)
                                                 name: UIDeviceOrientationDidChangeNotification
                                               object: nil];
}

- (void) viewWillAppear:(BOOL)animated
{
    [[ToolBox shared]setTabBarTint:self.tabBarController];
    [self reload];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void) dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver: self];
    [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
}

#pragma mark - Orientation change notification

- (void)deviceOrientationDidChange:(NSNotification *)notification {
//    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    [self.tblMeditate reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dicRecords.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

//    static NSString *simpleTableIdentifier = @"BarTableCell2";
    
    @try {
        if (![[NSBundle mainBundle] loadNibNamed:@"BarTableCell" owner:self options:nil])
        {
            NSLog(@"Warning! Could not load myNib file.\n");
            return NO;
        }
    }
    @catch (NSException* ex)
    {
        NSLog(@"%@", [ex reason]);
        return nil;
    }
    
    BarTableCell *cell = nil;
//    cell = (BarTableCell *)[tableView dequeueReusableCellWithIdentifier:@"BarTableCell"];
    
    if (cell == nil)
    {
        NSArray *nib;
        nib = [[NSBundle mainBundle] loadNibNamed:@"BarTableCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    else
    {
    }

    if ( indexPath.row == 0 )
        [cell.labelToday setHidden:FALSE];
    else
        [cell.labelToday setHidden:TRUE];
    
//    if ( ! [self.records count] )
//        return cell;

    NSInteger ind = ([self.dicRecords count] - indexPath.row - 1);
    NSNumber *rec;
    NSString *key;

    NSArray *keys = [self.dicRecords allKeys];
    keys = [[self.dicRecords allKeys] sortedArrayUsingSelector: @selector(compare:)];

    @try {
        key = [keys objectAtIndex:ind];
    }
    @catch (NSException* ex) {
        NSLog(@"%@", [ex reason]);
        return cell;
    }
    
    rec = [self.dicRecords objectForKey:key];
   
    if ( [rec integerValue] )
        cell.labelTimeDone.text = [[ToolBox shared] secondsToTimeStrShort:[rec integerValue]];
    else
        cell.labelTimeDone.text = @"";
    
    NSString *labelDate;
    labelDate = [key substringWithRange:NSMakeRange(5,5)];
    cell.labelDate.text = labelDate;

    NSDate *tmpDate;
    tmpDate = [[ToolBox shared]dateFromStr:key];
    
    cell.labelDayOfWeek.text = [[ToolBox shared]dayOfWeek:tmpDate];
    
    int dow = [[ToolBox shared]dayOfWeekNr:tmpDate];

    if ( indexPath.row == 0 )
        cell.backgroundColor = [UIColor colorWithRed:247.0f/256.0f
                                               green:229.0f/256.0f
                                                blue:23.0f/256.0f
                                               alpha:0.45];
    else if ( dow == 7 || dow == 1 )
        cell.backgroundColor = [UIColor colorWithRed:0
                                               green:0
                                                blue:0.5
                                               alpha:0.15];
    else
        cell.backgroundColor = [UIColor colorWithRed:247.0f/256.0f
                                               green:229.0f/256.0f
                                                blue:23.0f/256.0f
                                               alpha:0.20];

    [cell setTimeDone:[rec integerValue]];
    [cell setMaxTimeDone:[self.maxTimeDone integerValue]];
    [cell setIndex:indexPath.row + 1];
    
    [cell setNeedsLayout];

    return cell;
}

//------------------
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 24;
}

//------------------
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
//    return NO;
    return NO;
}

//------------------
-  (void)tableView:(UITableView *)tableView
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
 forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        // Delete the row from the data source
//        [[StorageManager sharedManager] deleteRecord:indexPath.row];
//        [self.tblMeditate deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
//        [self.tblMeditate reloadData];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
//        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - My
-(void) reload {
    [self.tblMeditate reloadData];
    
    self.rawRecords = [[StorageManager sharedManager]fetchData];
    self.records = [[StorageManager sharedManager]fetchData];
    
    self.maxTimeDone = 0;
    
    NSMutableDictionary *tmpDic;
    tmpDic = [[NSMutableDictionary alloc]init];
    
    for( int i=0; i<[self.rawRecords count]; i++)
    {
        NSObject *r = [self.rawRecords objectAtIndex:i];
        NSNumber *timeStart     = [r valueForKey:@"timeStart"] ;
        NSNumber *timeDone      = [r valueForKey:@"timeDone"] ;
        NSString *key;
        key = [[ToolBox shared]secondsToDateStr:[timeStart integerValue]];
        
        NSNumber *value = [tmpDic objectForKey:key];
        if (value)
        {
            NSNumber *totalTime;
            totalTime = [NSNumber numberWithInteger:([value integerValue] + [timeDone integerValue])];
            [tmpDic setObject:totalTime  forKey:key];
            
            if ( [totalTime integerValue] > [self.maxTimeDone integerValue] )
                self.maxTimeDone = totalTime;
        }
        else
        {
            [tmpDic setObject:timeDone forKey:key];
            if ( [timeDone integerValue] > [self.maxTimeDone integerValue] )
                self.maxTimeDone = timeDone;
        }

    }
    
    [self.dicRecords removeAllObjects];

    for (int i = 0; i<34; i++)
    {
        NSDate *date = [NSDate dateWithTimeIntervalSinceNow:(-i*24*60*60)];
        NSString *sdate = [[ToolBox shared]dateToString:date];
        NSNumber *value = [tmpDic objectForKey:sdate];
        if (!value)
            [self.dicRecords setObject:[NSNumber numberWithInteger:0] forKey:sdate];
        else
            [self.dicRecords setObject:[tmpDic objectForKey:sdate] forKey:sdate];
    }
}

//- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
//    //UIGraphicsBeginImageContext(newSize);
//    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
//    // Pass 1.0 to force exact pixel size.
//    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
//    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
//    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return newImage;
//}

@end
