//
//  BarGraphViewController.m
//  Budda Friend
//
//  Created by Przemek Urbanski on 02/12/14.
//  Copyright (c) 2014 Przemek Urbanski. All rights reserved.
//

#import "BarGraphViewController.h"
#import "StatsManager.h"
#import "ToolBox.h"
#import "AudioManager.h"

@interface BarGraphViewController ()

@property (nonatomic, strong) CPTGraphHostingView *hostView;
@property (nonatomic, strong) CPTBarPlot *aaplPlot;
@property (nonatomic, strong) CPTBarPlot *googPlot;
@property (nonatomic, strong) CPTBarPlot *msftPlot;
@property (nonatomic, strong) CPTPlotSpaceAnnotation *priceAnnotation;
@property (nonatomic) NSInteger daysCount;

//-(IBAction)aaplSwitched:(id)sender;
//-(IBAction)googSwitched:(id)sender;
//-(IBAction)msftSwitched:(id)sender;

-(void)initPlot;
-(void)configureGraph;
-(void)configurePlots;
-(void)configureAxes;
-(void)hideAnnotation:(CPTGraph *)graph;

@end


@implementation BarGraphViewController

CGFloat const CPDBarWidth = 0.25f;
CGFloat const CPDBarInitialX = 0.25f;

@synthesize hostView    = hostView_;
@synthesize aaplPlot    = aaplPlot_;
@synthesize googPlot    = googPlot_;
@synthesize msftPlot    = msftPlot_;
@synthesize priceAnnotation = priceAnnotation_;

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [[AudioManager shared]playSound:eSoundTap];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self initPlot];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Chart behavior
-(void)initPlot {
    self.daysCount = 31;
    [self configureHost];
    [self configureGraph];
    [self configurePlots];
    [self configureAxes];
}

-(void)configureHost {
    self.hostView = [(CPTGraphHostingView *) [CPTGraphHostingView alloc] initWithFrame:self.view.bounds];
    self.hostView.allowPinchScaling = NO;
    [self.view addSubview:self.hostView];
    return;
    
//    // 1 - Set up view frame
//    CGRect parentRect = self.view.bounds;
//    parentRect = CGRectMake(parentRect.origin.x,
//                            (parentRect.origin.y ),// + toolbarSize.height),
//                            parentRect.size.width,
//                            (parentRect.size.height ));//- toolbarSize.height));
//    // 2 - Create host view
//    self.hostView = [(CPTGraphHostingView *) [CPTGraphHostingView alloc] initWithFrame:self.view.bounds];
//
//    self.hostView.allowPinchScaling = YES;
//    [self.view addSubview:self.hostView];
}


-(void)configureGraph {
    // 1 - Create the graph
    CPTGraph *graph = [[CPTXYGraph alloc] initWithFrame:self.hostView.bounds];
    graph.plotAreaFrame.masksToBorder = NO;
    self.hostView.hostedGraph = graph;

    // 2 - Configure the graph
    [graph applyTheme:[CPTTheme themeNamed:kCPTStocksTheme]];
    graph.paddingBottom = 30.0f;
    graph.paddingLeft  = 40.0f;
    graph.paddingTop    = 0.0f;
    graph.paddingRight  = 0.0f;
    
    // 3 - Set up styles
    CPTMutableTextStyle *titleStyle = [CPTMutableTextStyle textStyle];
    titleStyle.color = [CPTColor whiteColor];
    titleStyle.fontName = @"Helvetica-Bold";
    titleStyle.fontSize = 16.0f;
    
    // 4 - Set up title
    NSString *title = @"Duration of meditation in minutes";
    graph.title = title;
    graph.titleTextStyle = titleStyle;
    graph.titlePlotAreaFrameAnchor = CPTRectAnchorTop;
    graph.titleDisplacement = CGPointMake(0.0f, -15.0f);
    
    // 5 - Set up plot space
    CGFloat xMin = 0.0f;
    CGFloat xMax = 300;
    CGFloat yMin = 0.0f;
    CGFloat yMax = 30.0f;  // should determine dynamically based on max price
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
    
    NSNumber *xMinN = [NSNumber numberWithFloat:xMin];
    NSNumber *xMaxN = [NSNumber numberWithFloat:xMax];
    NSNumber *yMinN = [NSNumber numberWithFloat:yMin];
    NSNumber *yMaxN = [NSNumber numberWithFloat:yMax];
    
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:xMinN length:xMaxN];
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:yMinN length:yMaxN];
    plotSpace.allowsUserInteraction = YES;
}

-(void)configurePlots {
    // 1 - Set up the three plots
    self.aaplPlot = [CPTBarPlot tubularBarPlotWithColor:[CPTColor redColor] horizontalBars:YES];
    self.aaplPlot.identifier = @"1";
//
//    self.googPlot = [CPTBarPlot tubularBarPlotWithColor:[CPTColor greenColor] horizontalBars:NO];
//    self.googPlot.identifier = @"2";
//    
//    self.msftPlot = [CPTBarPlot tubularBarPlotWithColor:[CPTColor blueColor] horizontalBars:NO];
//    self.msftPlot.identifier = @"3";
    
    // 2 - Set up line style
    CPTMutableLineStyle *barLineStyle = [[CPTMutableLineStyle alloc] init];
    barLineStyle.lineColor = [CPTColor redColor];
    barLineStyle.lineWidth = 0.5;
    
    // 3 - Add plots to graph
    CPTGraph *graph = self.hostView.hostedGraph;
//    self.hostView.hostedGraph = graph;

    CGFloat barX = CPDBarInitialX;
//    NSArray *plots = [NSArray arrayWithObjects:self.aaplPlot, self.googPlot, self.msftPlot, nil];
    NSArray *plots = [NSArray arrayWithObjects:self.aaplPlot, nil];
    for (CPTBarPlot *plot in plots) {
        plot.dataSource = self;
        plot.delegate = self;

        plot.barWidth = [NSNumber numberWithDouble:0.5];
        plot.barOffset = [NSNumber numberWithDouble: (barX+0.30)];
        plot.lineStyle = barLineStyle;
        [graph addPlot:plot toPlotSpace:graph.defaultPlotSpace];
//        [graph addPlot:plot ];

        barX += 0.5;
    }

}

-(void)configureAxes {
    // 1 - Configure styles
    CPTMutableTextStyle *axisTitleStyle = [CPTMutableTextStyle textStyle];
    axisTitleStyle.color = [CPTColor whiteColor];
    axisTitleStyle.fontName = @"Helvetica-Bold";
    axisTitleStyle.fontSize = 12.0f;
    CPTMutableLineStyle *axisLineStyle = [CPTMutableLineStyle lineStyle];
    axisLineStyle.lineWidth = 2.0f;
    axisLineStyle.lineColor = [[CPTColor whiteColor] colorWithAlphaComponent:1];
    
    // 2 - Get the graph's axis set
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *) self.hostView.hostedGraph.axisSet;
    
    // 3 - Configure the x-axis
    axisSet.xAxis.labelingPolicy = CPTAxisLabelingPolicyNone;
    axisSet.xAxis.title = @"";
//    axisSet.xAxis.titleTextStyle = axisTitleStyle;
//    axisSet.xAxis.titleOffset = 4.0f;
//    axisSet.xAxis.axisLineStyle = axisLineStyle;
    axisSet.xAxis.majorTickLength = 0;
    axisSet.xAxis.minorTickLength = 0;

    // 4 - Configure the y-axis
    axisLineStyle.lineColor = [[CPTColor whiteColor] colorWithAlphaComponent:0];

    axisSet.yAxis.labelingPolicy = CPTAxisLabelingPolicyAutomatic;
    axisSet.yAxis.title = @"";
    axisSet.yAxis.titleTextStyle = axisTitleStyle;
    axisSet.yAxis.titleOffset = 4.0f;
    axisSet.yAxis.axisLineStyle = axisLineStyle;
    axisSet.yAxis.majorTickLength = 0;
    axisSet.yAxis.minorTickLength = 0;
    
//    axisSet.yAxis.titleTextStyle = axisTitleStyle;
//    axisSet.yAxis.titleOffset = 5.0f;
//    axisSet.yAxis.axisLineStyle = axisLineStyle;
    /* --Define some custom labels for the data elements-- */
//     Note that x is defined as: CPTXYAxisSet *axisSet = (CPTXYAxisSet*)self.graph.axisSet;
    CPTXYAxis *x = axisSet.yAxis;
    x.labelingPolicy = CPTAxisLabelingPolicyNone; // This allows us to create custom axis labels for x axis
    NSMutableArray *ticks = [NSMutableArray arrayWithCapacity:1];
    NSMutableArray *_axisLabelStrings = [NSMutableArray arrayWithCapacity:self.daysCount+1];
    for (int i = 0 ; i< self.daysCount; i++)
    {
        NSString *s = [[ToolBox shared]dateAgoToStr:self.daysCount - i - 1];
        if ( i % 2 == 0)
            _axisLabelStrings[i] = [s substringWithRange:NSMakeRange(5,5)];
        else
            _axisLabelStrings[i] = @"";
    }
    for(unsigned int counter = 0; counter < [_axisLabelStrings count];counter++) {
        // Here the instance variable _axisLabelStrings is a list of custom labels
        [ticks addObject:[NSNumber numberWithInt:counter]];
    }
    NSUInteger labelLocation = 0;
    NSMutableArray* customLabels = [NSMutableArray arrayWithCapacity:[_axisLabelStrings count]];
    @try {
        for (NSNumber *tickLocation in ticks) {
            CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithText: [_axisLabelStrings objectAtIndex:labelLocation++] textStyle:x.labelTextStyle];
            newLabel.tickLocation =  [NSNumber numberWithFloat:[tickLocation floatValue ] + 1];
            newLabel.offset = 2.0f;//x.labelOffset + x.majorTickLength could be useful here.
            newLabel.alignment = CPTAlignmentBottom;
//            newLabel.rotation = M_PI/3.5f;
            [customLabels addObject:newLabel];
//            [newLabel release];
        }
    }
    @catch (NSException * e) {
        NSLog(@"An exception occurred while creating date labels for x-axis");
    }
    @finally {
        x.axisLabels =  [NSSet setWithArray:customLabels];
    }
    x.majorTickLocations = [NSSet setWithArray:ticks];
}

-(void)hideAnnotation:(CPTGraph *)graph {
}

#pragma mark - CPTPlotDataSource methods
-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot {
    return self.daysCount ;
//    NSUInteger size;
//    
//    size = [[[StatsManager sharedManager ] getStats ] count];
//    NSLog(@"Size: %d", size);
//
//    return size;
}

-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index {
    if ( ! fieldEnum )
        return [NSNumber numberWithInt:(self.daysCount - index)] ;

    NSArray *data;
    NSNumber *ret;
    NSString *date;
    
    data = [[StatsManager sharedManager] getStats];
    date = [[ToolBox shared] dateAgoToStr:index];
    ret = [NSNumber numberWithFloat:[[data valueForKey:date] floatValue]/ 60.0f] ;

//    NSLog(@"d: %d %d %@ %@ ", fieldEnum, index, ret, date);
    return ret;
}
//
//-(CPTLayer *)dataLabelForPlot:(CPTPlot *)plot recordIndex:(NSUInteger)index {
//    // 1 - Define label text style
//    static CPTMutableTextStyle *labelText = nil;
//    if (!labelText) {
//        labelText= [[CPTMutableTextStyle alloc] init];
//        labelText.color = [CPTColor grayColor];
//    }
//    // 2 - Calculate portfolio total value
//    NSDecimalNumber *portfolioSum = [NSDecimalNumber decimalNumberWithString:@"137" ];
//    //    for (NSDecimalNumber *price in [[CPDStockPriceStore sharedInstance] dailyPortfolioPrices]) {
//    //        portfolioSum = [portfolioSum decimalNumberByAdding:price];
//    //    }
//    // 3 - Calculate percentage value
//    NSDecimalNumber *price = [NSDecimalNumber decimalNumberWithString:@"63" ];
//    //[[[CPDStockPriceStore sharedInstance] dailyPortfolioPrices] objectAtIndex:index];
//    NSDecimalNumber *percent = [price decimalNumberByDividingBy:portfolioSum];
//    // 4 - Set up display label
//    NSString *labelValue = [NSString stringWithFormat:@"$%0.2f USD (%0.1f %%)", [price floatValue], ([percent floatValue] * 100.0f)];
//    NSString *date =  [[ToolBox shared] dateAgoToStr:index];
//    NSNumber *n = [[[StatsManager sharedManager] getStats] valueForKey:date];
//    if ( [n integerValue] > 1 )
//        labelValue =[ NSString stringWithFormat:@"%.1f" , [n floatValue] / 60.0f ];
//    else
//        labelValue = @"";
//   
////    labelValue =  [[ToolBox shared] dateAgoToStr:index];
//    
//    // 5 - Create and return layer with label text
//    CPTTextLayer *layer;
//    @try {
//        layer = [[CPTTextLayer alloc] initWithText:labelValue style:labelText];
//    }
//    @catch (NSException *e)
//    {
//        NSLog(@"%@", [e reason]);
//    }
//    
//    return layer;
//}

#pragma mark - CPTBarPlotDelegate methods
-(void)barPlot:(CPTBarPlot *)plot barWasSelectedAtRecordIndex:(NSUInteger)index {
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
