//
//  BarTableController.h
//  Budda Friend
//
//  Created by Przemek Urbanski on 31/12/14.
//  Copyright (c) 2014 Przemek Urbanski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BarTableController : UITableViewController

@property (strong, nonatomic) IBOutlet UITableView *tblMeditate;

@end
