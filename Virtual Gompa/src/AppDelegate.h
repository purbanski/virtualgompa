//
//  AppDelegate.h
//  Budda Friend
//
//  Created by Przemek Urbanski on 24/11/14.
//  Copyright (c) 2014 Przemek Urbanski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "ProfilesController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ProfilesController *viewController;
@property NSUInteger orientationMask;

@end

