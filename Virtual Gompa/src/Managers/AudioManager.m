//
//  AudioManager.m
//  Budda Friend
//
//  Created by Przemek Urbanski on 01/12/14.
//  Copyright (c) 2014 Przemek Urbanski. All rights reserved.
//

#import "AudioManager.h"
@interface AudioManager ()

@property NSMutableArray *sounds;
@property (readonly)	SystemSoundID	soundTap;
@property (readonly)	SystemSoundID	soundMeditateFinish;

@end

@implementation AudioManager

static AudioManager *sharedAudioManager = nil;

+ (id)shared {
    if (sharedAudioManager == nil ) {
        sharedAudioManager = [[ super allocWithZone:NULL] init];
    }
    return sharedAudioManager;
}

- (id)init {
    if (self = [super init]) {
        [self initSounds];
    }
    return self;
}

-(void) initSounds {
    self.sounds = [[NSMutableArray alloc]init];

    [self initSound:eSoundTap filename:@"tap" extension:@"aif"];
    [self initSound:eSoundMedFinish filename:@"medFinish" extension:@"aif"];
}

-(void) initSound:(NSInteger)soundEnum
         filename:(NSString*)filename
        extension:(NSString*)extension
{
    SystemSoundID sound;
    [self loadSound:filename ext:extension sound:&sound];
    NSNumber *snd = [NSNumber numberWithLong:sound];
    [self.sounds insertObject:snd atIndex:soundEnum];
}

-(void)loadSound:(NSString *)filename
             ext:(NSString*)ext
           sound:(SystemSoundID*)sound
{
    NSURL *soundURL;
    soundURL = [[NSBundle mainBundle] URLForResource: filename
                                    withExtension: ext];
    
    AudioServicesCreateSystemSoundID ((__bridge CFURLRef)soundURL, sound);
    
}

-(void)dealloc {
    [self.sounds removeAllObjects];
//    [self.sounds release];
}

#pragma mark sounds

-(void) playSound:(NSInteger)sound
{
    if (sound == eSoundTap )
        return;
    
    SystemSoundID s;
    s = [[self.sounds objectAtIndex:sound] integerValue];
    
    AudioServicesPlaySystemSound(s);
}


@end
