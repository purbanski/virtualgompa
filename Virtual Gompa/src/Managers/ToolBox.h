//
//  ToolBox.h
//  Budda Friend
//
//  Created by Przemek Urbanski on 29/11/14.
//  Copyright (c) 2014 Przemek Urbanski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface ToolBox : NSObject

+ (id)shared;

- (NSArray *) secondsToTime:(NSUInteger) totalSeconds;
- (NSString *) secondsToTimeStr:(NSUInteger) totalSeconds;
- (NSString *) secondsToTimeStrShort:(NSUInteger) totalSeconds;
- (NSString *) secondsToDateStr:(NSUInteger) timeStart;

- (NSDate*) dateFromStr:(NSString*)date;

- (NSString *) dayOfWeek:(NSDate *)date;
- (NSInteger) dayOfWeekNr:(NSDate *)date;

- (NSString *) addLeadingZero:(NSNumber *) num;
- (NSDictionary *)dateToDictionary:(NSDate *) date;
- (NSString *)dateToString:(NSDate *) date;
- (NSString *) dateAgoToStr:(NSUInteger) dayAgo;

- (void)scheduleAlarmForDate:(NSDate*)theDate;
- (void)setupNotificationSettings;
- (void)cancelScheduleNotifications;
- (void)delayScheduledNotification:(NSInteger) delaySec;

- (void) dumpDate:(NSDate*) date;


-(void) animateOut:(UIView *) obj delay:(NSInteger) delay ;
-(void) animateIn:(UIView *) obj delay:(NSInteger) delay;
-(void) animateIn:(UIView *) label;

-(void)makeTabBarHidden:(BOOL)hide tabBarController:(UITabBarController*)tabBarController;

-(void) setTabBarTint:(UITabBarController*)tabBarController;
@end
