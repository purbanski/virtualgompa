//
//  StorageManager.m
//  Budda Friend
//
//  Created by Przemek Urbanski on 29/11/14.
//  Copyright (c) 2014 Przemek Urbanski. All rights reserved.
//

#import "StorageManager.h"
#import "ToolBox.h"

@implementation StorageManager

static StorageManager *sharedManager = nil;

+ (id)sharedManager {
    if (sharedManager == nil ) {
        sharedManager = [[ super allocWithZone:NULL] init];
    }
    return sharedManager;
}

- (id)init {
    if (self = [super init]) {
//        someProperty = [[NSString alloc] initWithString:@"Default Property Value"];
    }
    return self;
}

- (void) insertTimeStart:(NSInteger)timeStart timePlanned:(NSInteger)timePlanned timeDone:(NSInteger)timeDone
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSManagedObject *meditation = [NSEntityDescription insertNewObjectForEntityForName:@"Meditation"
                                                                inManagedObjectContext:context];
    
    [meditation setValue:[NSNumber numberWithInteger:timeStart]     forKey:@"timeStart"];
    [meditation setValue:[NSNumber numberWithInteger:timeDone]      forKey:@"timeDone"];
    [meditation setValue:[NSNumber numberWithInteger:timePlanned]   forKey:@"timePlanned"];
    
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
}

- (void) insert:(NSInteger)timeStart :(NSInteger)timePlanned :(NSInteger)timeDone {
    [self insertTimeStart:timeStart timePlanned:timePlanned timeDone:timeDone];
}

- (void) deleteRecord:(NSInteger)ind
{
    NSArray *records;
    NSError *error;
    NSManagedObjectContext *context;

    records = [self fetchData];
    context = [self managedObjectContext];
    
    if (ind + 1 <= [records count])
    {
        [context deleteObject:[records objectAtIndex:ind]];
        if (![context save:&error]) {
            // Handle the error.
        }
    }
}

- (void) dumpData {
    NSArray *fetchedObjects = [self fetchData];

    for (NSManagedObject *info in fetchedObjects) {
        NSDate *dateStart = [NSDate dateWithTimeIntervalSince1970:[[info valueForKey:@"timeStart"] integerValue] ];
//        NSDictionary *dic = [[ToolBox shared] dateToDictionary:dateStart];
//        NSInteger timeStart =[[ info valueForKey:@"timeStart"] integerValue];
        
        [[ToolBox shared] dumpDate: dateStart];
        NSLog(@"Name: %@", [info valueForKey:@"timeStart"]);
        NSLog(@"Name: %@", [info valueForKey:@"timeDone"]);
        NSLog(@"Name: %@", [info valueForKey:@"timePlanned"]);
        NSLog(@"------------------");
    }

}

- (NSArray*) fetchData {
    NSError *error;
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"timeStart" ascending:NO];
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Meditation" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    return fetchedObjects;
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}


-(void)dealloc {
}


#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.blackted.budda-friend.test" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"VirtualGompa" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"VirtualGompa.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}


@end
