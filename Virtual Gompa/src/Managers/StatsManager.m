//
//  StorageManager.m
//  Budda Friend
//
//  Created by Przemek Urbanski on 29/11/14.
//  Copyright (c) 2014 Przemek Urbanski. All rights reserved.
//

#import "StorageManager.h"
#import "StatsManager.h"
#import "ToolBox.h"

@implementation StatsManager

static StatsManager *sharedManager = nil;

+ (id)sharedManager {
    if (sharedManager == nil ) {
        sharedManager = [[ super allocWithZone:NULL] init];
    }
    return sharedManager;
}

- (id)init {
    if (self = [super init]) {

    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

-(void)dealloc {
}

-(NSDictionary *) getStats {
    NSMutableArray *arr = [NSMutableArray new];
    NSMutableDictionary *stats = [NSMutableDictionary new];
    
    NSUInteger count = 0;
    
    NSArray *data = [[StorageManager sharedManager] fetchData];
    for (NSManagedObject *info in data)
    {
        NSNumber *timeDone = [info valueForKey:@"timeDone"] ;
        NSNumber *timeStart = [info valueForKey:@"timeStart"] ;
        NSString *date = [[ToolBox shared] secondsToDateStr:[timeStart integerValue]];

        if ( !timeStart )
            continue;
        
        if ( [stats valueForKey:date ] == nil )
            [stats setValue:[NSNumber numberWithInt:0]
                     forKey:date ];

        int secTotal = [[stats valueForKey:date] integerValue]+ [timeDone integerValue];
        
        [stats setValue: [NSNumber numberWithInt:secTotal]
                 forKey:date];
        

        count++;
    }
    
    count  = 0;
    for (id key in stats) {
        NSDictionary *d;
        d = [stats objectForKey:key];
        if ( d < 0 )
            continue;
        
        arr[count] = @{@"date" : key, @"time": d };
        count++;
//        NSLog(@"key: %@, value: %@ \n", key, [NSNumber numberWithInt:[[stats objectForKey:key] integerValue]]);
    }

    return stats;
}

@end
