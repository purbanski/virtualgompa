//
//  AudioManager.h
//  Budda Friend
//
//  Created by Przemek Urbanski on 01/12/14.
//  Copyright (c) 2014 Przemek Urbanski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>

NS_ENUM(NSInteger, MySound) {
    eSoundTap = 0,
    eSoundMedFinish
};

@interface AudioManager : NSObject

//- (void) playSoundTap;
//- (void) playSoundMedFinish;
- (void) playSound:(NSInteger) sound;
//- (IBAction) vibrate: (id) sender;

+ (id)shared;

@end
