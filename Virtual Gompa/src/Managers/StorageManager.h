//
//  StorageManager.h
//  Budda Friend
//
//  Created by Przemek Urbanski on 29/11/14.
//  Copyright (c) 2014 Przemek Urbanski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface StorageManager : NSObject {
}

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (id)sharedManager;
- (void) insert:(NSInteger)timeStart :(NSInteger)timePlanned :(NSInteger)timeDone;
- (void) insertTimeStart:(NSInteger)timeStart timePlanned:(NSInteger)timePlanned timeDone:(NSInteger)timeDone;

- (void) dumpData;
- (NSArray*) fetchData;
- (void) deleteRecord:(NSInteger)ind;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
