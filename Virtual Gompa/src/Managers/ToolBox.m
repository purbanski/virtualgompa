//
//  ToolBox.m
//  Budda Friend
//
//  Created by Przemek Urbanski on 29/11/14.
//  Copyright (c) 2014 Przemek Urbanski. All rights reserved.
//

#import "ToolBox.h"

@implementation ToolBox

static ToolBox *sharedToolBox = nil;

+ (id)shared {
    if (sharedToolBox == nil ) {
        sharedToolBox = [[ super allocWithZone:NULL] init];
    }
    return sharedToolBox;
}

- (id)init {
    if (self = [super init]) {
    }
    return self;
}

-(void)dealloc {
}

#pragma mark Time releated
- (NSArray *) secondsToTime:(NSUInteger) totalSeconds
{
    NSNumber *seconds   = [NSNumber numberWithInteger:(totalSeconds % 60)];
    NSNumber *minutes   = [NSNumber numberWithInteger:((totalSeconds / 60) % 60)];
    NSNumber *hours     = [NSNumber numberWithInteger:(totalSeconds / 3600)];
    
    NSArray *time = @[seconds, minutes, hours ];
    return time;
}

- (NSString *) secondsToTimeStr:(NSUInteger) totalSeconds
{
    // gmt fix should not be here
    totalSeconds += [[NSTimeZone systemTimeZone] secondsFromGMT];

    NSInteger seconds   = [[NSNumber numberWithInteger:(totalSeconds % 60)] integerValue];
    NSInteger minutes   = [[NSNumber numberWithInteger:((totalSeconds / 60) % 60)] integerValue];
    NSInteger hours     = [[NSNumber numberWithInteger:(totalSeconds / 3600) % 24] integerValue];
    NSString *time      = [NSString stringWithFormat:@"%02d:%02d:%02d", hours, minutes, seconds ];
    return time;
}

- (NSString *) secondsToTimeStrShort:(NSUInteger)totalSeconds
{
    NSInteger minutes   = [[NSNumber numberWithInteger:((totalSeconds / 60) % 60)] integerValue];
    NSInteger hours     = [[NSNumber numberWithInteger:(totalSeconds / 3600) % 24] integerValue];
    NSString *time      = [NSString stringWithFormat:@"%02d:%02d", hours, minutes ];
    return time;
}

- (NSString *) secondsToDateStr:(NSUInteger) timeStart
{
    NSDate *dateStart = [NSDate dateWithTimeIntervalSince1970:timeStart];
    return [self dateToString:dateStart];
}

- (NSString *) dateAgoToStr:(NSUInteger) dayAgo
{
    NSTimeInterval seconds = [[NSDate date] timeIntervalSince1970];
    seconds -= (dayAgo * 24 * 60 * 60 );
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:seconds];
    return [self dateToString:date];
}

- (NSDictionary *)dateToDictionary:(NSDate *) date
{
    int comp = NSCalendarUnitYear| NSCalendarUnitMonth| NSCalendarUnitDay;
    comp |= NSCalendarUnitHour | NSCalendarUnitMinute| NSCalendarUnitSecond;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:comp
                                               fromDate:date];
    NSNumber *hour = [NSNumber numberWithInteger: [components hour]];
    NSNumber *minute = [NSNumber numberWithInteger:[components minute]];
    NSNumber *second = [NSNumber numberWithInteger:[components second]];
    
    NSNumber *year = [NSNumber numberWithInteger: [components year]];
    NSNumber *month = [NSNumber numberWithInteger:[components month]];
    NSNumber *day = [NSNumber numberWithInteger:[components day]];
    
    
    NSDictionary *dic = @{ @"year" : year,
                           @"month" : month,
                           @"day" : day,
                           @"hour" : hour,
                           @"minute" : minute,
                           @"second" : second };
    return dic;
}

- (NSString *)dateToString:(NSDate *) date
{
    int comp = NSCalendarUnitYear| NSCalendarUnitMonth| NSCalendarUnitDay;
    comp |= NSCalendarUnitHour | NSCalendarUnitMinute| NSCalendarUnitSecond;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:comp
                                               fromDate:date];

//    NSNumber *hour = [NSNumber numberWithInteger: [components hour]];
//    NSNumber *minute = [NSNumber numberWithInteger:[components minute]];
//    NSNumber *second = [NSNumber numberWithInteger:[components second]];
    
    NSNumber *year = [NSNumber numberWithInteger: [components year]];
    NSNumber *month = [NSNumber numberWithInteger:[components month]];
    NSNumber *day = [NSNumber numberWithInteger:[components day]];
    
    
    NSString *ret;
    ret = [ NSString stringWithFormat:@"%@-%02d-%02d" , year, [month integerValue], [day integerValue]];
    return ret;
}


-(void) dumpDate:(NSDate*) date
{
    NSDictionary *dic = [self dateToDictionary:date];
    NSLog(@"date: %d-%02d-%02d %02d:%02d:%02d",
          [[dic valueForKey:@"year"]integerValue],
          [[dic valueForKey:@"month"] integerValue],
          [[dic valueForKey:@"day"] integerValue],
          [[dic valueForKey:@"hour"] integerValue],
          [[dic valueForKey:@"minute"] integerValue],
          [[dic valueForKey:@"second"] integerValue]);
}

-(NSString*) dayOfWeek:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setDateFormat:@"EE"];
//    [dateFormatter setDateFormat:@"EEEE"];
    return [dateFormatter stringFromDate:date];
}

-(NSInteger) dayOfWeekNr:(NSDate*) date
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *comps = [gregorian components:NSCalendarUnitWeekday fromDate:date];
//    NSLog(@"%d", [comps weekday]);
    return [comps weekday];
}

-(NSDate*) dateFromStr:(NSString *)dateStr
{
//    NSString *dateStr = @"20100223";
    
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    return date;
    // Convert date object to desired output format
//    [dateFormat setDateFormat:@"EEEE MMMM d, YYYY"];
//    dateStr = [dateFormat stringFromDate:date];
//    return dateStr;
}
#pragma mark Formating
- (NSString *) addLeadingZero:(NSNumber *)num
{
    NSString *str;
    str = [ NSString stringWithFormat:@"%02d", [num integerValue] ];
    return str;
}

#pragma mark - Notificaton
- (void)scheduleAlarmForDate:(NSDate*)theDate
{
    UIApplication* app;
    app = [UIApplication sharedApplication];

    // Create a new notification.
    UILocalNotification* alarm = [[UILocalNotification alloc] init];
    if (alarm)
    {
        alarm.fireDate = theDate;
        alarm.timeZone = [NSTimeZone defaultTimeZone];
        alarm.repeatInterval = 0;
        alarm.soundName = @"medFinish.aif";
        alarm.alertBody = @"Be happy :)";
        
        //        sound   = [[NSBundle mainBundle] URLForResource: @""
        //                                          withExtension: @"aif"];
        [app scheduleLocalNotification:alarm];
    }
}

- (void)setupNotificationSettings
{
    UIUserNotificationType notificationTypes;
    UIUserNotificationSettings *notifcationSettings;
    
    notificationTypes = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    notifcationSettings = [UIUserNotificationSettings settingsForTypes:notificationTypes categories:nil];
    
    [[UIApplication sharedApplication] registerUserNotificationSettings:notifcationSettings];
}

-(void)cancelScheduleNotifications
{
    UIApplication *app      = [UIApplication sharedApplication];
    NSArray *notifications  = [app scheduledLocalNotifications];
    
    // Clear out the old notification before scheduling a new one.
    if ([notifications count] > 0)
        [app cancelAllLocalNotifications];
}

-(void)delayScheduledNotification:(NSInteger) delaySec
{
    UIApplication *app      = [UIApplication sharedApplication];
    NSArray *notifications  = [app scheduledLocalNotifications];

    for (UILocalNotification* notify in notifications)
    {
        NSDate *date;
        date = notify.fireDate;
        
        date = [NSDate dateWithTimeInterval:delaySec sinceDate:notify.fireDate];
        notify.fireDate = date;
    }
    
}

#pragma mark - animate

-(void) animateOut:(UIView *) obj delay:(NSInteger) delay {
    [UIView animateWithDuration:0.25
                          delay:delay
                        options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                     animations:^(void)
     {
         [obj setAlpha:0.0];
     }
                     completion:^(BOOL finished)
     {
         if(finished)
         {
             [obj setHidden:TRUE];
         }
     }];
}
-(void) animateIn:(UIView *) obj delay:(NSInteger) delay {
    [obj setAlpha:0.0];
    [obj setHidden:false];
    [UIView animateWithDuration:0.25
                          delay:delay
                        options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                     animations:^(void)
     {
         [obj setAlpha:1.0];
     }
                     completion:^(BOOL finished)
     {
         if(finished)
         {
         }
     }];
}

-(void) animateIn:(UIView *) label {
    [label setAlpha:0.0];
    [label setHidden:false];
    [UIView animateWithDuration:1.0
                          delay:0
                        options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                     animations:^(void)
     {
         [label setAlpha:1.0];
     }
                     completion:^(BOOL finished)
     {}];
}


-(void)makeTabBarHidden:(BOOL)hide tabBarController:(UITabBarController*)tabBarController
{
    // Custom code to hide TabBar
    
    if ( [tabBarController.view.subviews count] < 2 ) {
        return;
    }
    
    UIView *contentView;
    
    if ( [[tabBarController.view.subviews objectAtIndex:0] isKindOfClass:[UITabBar class]] ) {
        contentView = [tabBarController.view.subviews objectAtIndex:1];
    } else {
        contentView = [tabBarController.view.subviews objectAtIndex:0];
    }
    
    if (hide) {
//        contentView.frame = CGRectMake(0,0,10,10);
        contentView.frame = tabBarController.view.bounds;
    }
    else {
        contentView.frame = CGRectMake(tabBarController.view.bounds.origin.x,
                                       tabBarController.view.bounds.origin.y,
                                       tabBarController.view.bounds.size.width,
                                       tabBarController.view.bounds.size.height - tabBarController.tabBar.frame.size.height);
    }
    
    tabBarController.tabBar.hidden = hide;
}

-(void) setTabBarTint:(UITabBarController*)tabBarController {
    [tabBarController.tabBar setTintColor:[UIColor colorWithRed:1 green:0.5 blue:0 alpha:1]];
}
@end
