//
//  StatsManager.h
//  Budda Friend
//
//  Created by Przemek Urbanski on 02/12/14.
//  Copyright (c) 2014 Przemek Urbanski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StatsManager : NSObject

+ (id)sharedManager;
-(NSArray *) getStats;

@end
