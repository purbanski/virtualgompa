//
//  Settings.m
//  Budda Friend
//
//  Created by Przemek Urbanski on 03/01/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#import "Settings.h"
@interface Settings ()

@property (nonatomic,weak) NSUserDefaults* defaults;

@end

@implementation Settings

static Settings *sharedManager = nil;

+ (id)shared {
    if (sharedManager == nil ) {
        sharedManager = [[ super allocWithZone:NULL] init];
    }
    return sharedManager;
}

-(id)init{
    if ([super init])
    {
        self.defaults = [NSUserDefaults standardUserDefaults];
        [self firstRun];
    }
    return self;
    
}

//-(void)setAlarms:(NSArray *)alarms
//{
//    [self.defaults setObject:alarms forKey:@"alarms"];
//    [self save];
//}

-(void)setProfiles:(NSDictionary *)profiles
{
    [self.defaults setObject:profiles forKey:@"profiles"];
    [self save];
}
-(NSDictionary*) getProfiles
{
    NSDictionary *profiles;
    profiles = [self.defaults objectForKey:@"profiles"];
    if (profiles==nil)
        profiles = [[NSDictionary alloc]init];
    
    return profiles;
}

-(void) save {
    [self.defaults synchronize];
}

-(void) removeProfileForKey:(NSString*) key {
    NSMutableDictionary *profiles;
    profiles = [[NSMutableDictionary alloc] initWithDictionary: [self.defaults objectForKey:@"profiles"]];
    [profiles removeObjectForKey:key];
    [self.defaults setObject:profiles forKey:@"profiles"];

    [self save];
}

-(BOOL) hasProfile:(NSString*) key
{
    id obj;
    obj = [[self.defaults objectForKey:@"profiles"] objectForKey:key];
    
    if (obj)
        return TRUE;
    return FALSE;
}
-(void) firstRun {
    if ([self.defaults objectForKey:@"first_run"] == nil)
    {
        [self.defaults setObject:@"done" forKey:@"first_run"];

        NSMutableDictionary *profiles;
        profiles = [[NSMutableDictionary alloc]init];
        
        [profiles setObject:@[@"3600"] forKey:@"1 hour"];
        [profiles setObject:@[@"1800"] forKey:@"30 minutes"];
        [profiles setObject:@[@"600"]  forKey:@"10 minutes"];
        [self setProfiles:profiles];
    }
}
@end
