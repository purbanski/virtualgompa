//
//  Settings.h
//  Budda Friend
//
//  Created by Przemek Urbanski on 03/01/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Settings : NSObject

+ (id)shared;
-(void) save;
//-(void) setAlarms:(NSArray*) alarms;
-(void) setProfiles:(NSDictionary*) profiles;

-(void) removeProfileForKey:(NSString*) key;

-(NSDictionary*) getProfiles;

-(BOOL) hasProfile:(NSString*) key;

@end
