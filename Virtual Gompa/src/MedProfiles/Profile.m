//
//  Profile.m
//  Budda Friend
//
//  Created by Przemek Urbanski on 03/01/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#import "Profile.h"
#import "Settings.h"
#import "ToolBox.h"

@interface Profile ()

@end

@implementation Profile

@synthesize profileName;
@synthesize alarms;

+(id) createWithName:(NSString*)profileName
            alarms:(NSArray*)alarms {
    Profile *p;
    p = [[Profile alloc]init];
    
    [p setAlarms:[NSMutableArray arrayWithArray:[alarms sortedArrayUsingSelector:@selector(compare:) ]]];
    [p setProfileName:profileName];
    
    return p;
}

#pragma mark - Profile
-(float) getLastAlarmTime
{
    if (! [self.alarms count ])
        return -1;
    
    NSNumber *n;
    n = [self.alarms objectAtIndex:([self.alarms count] -1 )];
    return [n floatValue];
}

-(NSString*) getLastAlarmTimeStr
{
    return [[ToolBox shared]secondsToTimeStr:[self getLastAlarmTime]];
}
//+(id) createWithIndex:(NSInteger)index
//{
//    Profile *p;
//    NSDictionary *profiles;
//    
//    p = [[Profile alloc]init];
//    profiles = Settings
//}

@end
