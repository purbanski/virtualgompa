//
//  MeditateProfileController.m
//  Budda Friend
//
//  Created by Przemek Urbanski on 04/01/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#import "MeditateProfileController.h"
#import "ToolBox.h"
#import "AudioManager.h"
#import "Settings.h"
#import "IntroTimerController.h"
#import "StorageManager.h"
#import "TableStatsViewController.h"

@interface MeditateProfileController ()

typedef NS_ENUM(NSUInteger, OperateMode) {
    kOpModeMeditate = 0,
    kOpModePaused
};

@property NSTimeInterval secStart;
@property NSTimeInterval secStartLast;
@property NSTimeInterval secDone;
@property NSTimeInterval secPaused;
//@property NSTimeInterval secLastPaused;

@property bool finishSndPlayed;

@property NSTimer *timer;
@property NSInteger alarmIndex;
@property OperateMode operateMode;

@end

//---------------------------------------------
//
//---------------------------------------------
@implementation MeditateProfileController

@synthesize profile;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.alarmIndex = 0;
    self.secDone = -1;
    self.secPaused = 0;
//    self.secLastPaused = 0;
    
    self.operateMode = kOpModeMeditate;
    [self.btnStop setHidden:TRUE];
    [self.btnPause setHidden:TRUE];
    [self.btnResume setHidden:TRUE];
    [self.btnDone setHidden:TRUE];
    
    self.lblProfileName.text = self.profile.profileName;
    self.lblProfileName.textAlignment = NSTextAlignmentCenter;

    self.finishSndPlayed = FALSE;
    [self updateTimerLabels:self.profile.getLastAlarmTime];
    [self updateAlarmLabels];
}

-(void)viewWillDisappear:(BOOL)animated {
//    [self.navigationController setNavigationBarHidden:NO animated:NO];
//    [[ToolBox shared]makeTabBarHidden:NO tabBarController:self.tabBarController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ( [[segue identifier] isEqualToString:@"idSegueIntroTimer" ] )
    {
        IntroTimerController *controller;
        controller = (IntroTimerController*) [segue destinationViewController];
        controller.delegate = self;
        [controller setSeconds:5];
    }
}

#pragma mark - my

-(void)updateTimerLabels:(NSInteger)sec
{
    NSArray *time;
    time = [[ToolBox shared] secondsToTime:sec];
    
    NSString *secStr = [[ToolBox shared] addLeadingZero:time[0] ];
    NSString *minStr = [[ToolBox shared] addLeadingZero:time[1] ];
    NSString *hourStr = [[ToolBox shared] addLeadingZero:time[2] ];
    
    [self.lblTimerSecond setText:secStr];
    [self.lblTimerMinute setText:minStr];
    [self.lblTimerHour setText:hourStr];
}

-(void)updateAlarmLabels
{
    if (self.alarmIndex >= [self.profile.alarms count] - 1 )
    {
//        if ( [self.profile.alarms count] == 0 )
//            [self.viewAlarmPanel setHidden:TRUE];
//        else
        [[ToolBox shared]animateOut:self.viewAlarmPanel delay:0.0];
        
        return;
    }
    if ( [self.viewAlarmPanel isHidden] )
        [self.viewAlarmPanel setHidden:FALSE];

    NSInteger alarmTime;
    alarmTime = [[self.profile.alarms objectAtIndex:self.alarmIndex] integerValue];
    alarmTime = [self.profile getLastAlarmTime] - alarmTime;

    NSArray *time;
    time = [[ToolBox shared] secondsToTime:alarmTime];
    
    NSString *secStr = [[ToolBox shared] addLeadingZero:time[0] ];
    NSString *minStr = [[ToolBox shared] addLeadingZero:time[1] ];
    NSString *hourStr = [[ToolBox shared] addLeadingZero:time[2] ];
    
    [self.lblNextAlarmSecond setText:secStr];
    [self.lblNextAlarmMinute setText:minStr];
    [self.lblNextAlarmHour setText:hourStr];

    self.lblAlarmCount.text = [NSString stringWithFormat:@"%d",[self.profile.alarms count] - self.alarmIndex - 1];
}

-(void) checkAlarm:(NSTimeInterval) secElapsed {
    if ( self.alarmIndex < [self.profile.alarms count] )
    {
        NSNumber *alarmTime;
        alarmTime = [self.profile.alarms objectAtIndex:self.alarmIndex];
        
        if (secElapsed >= [alarmTime floatValue] )
        {
            if ( !( secElapsed > [alarmTime floatValue] + 1.0f ))
                [[AudioManager shared]playSound:eSoundMedFinish];
            
            while (secElapsed>=[alarmTime floatValue]) {
                self.alarmIndex++;
                if (self.alarmIndex >= [self.profile.alarms count])
                    break;
                alarmTime = [self.profile.alarms objectAtIndex:self.alarmIndex];
            }
            
            [self updateAlarmLabels];
        }
    }
}

-(void) meditationHasFinished {
    UIColor *col1 = [UIColor colorWithRed:98.0/255.0
                                    green:186.0/255.0
                                     blue:32.0/255.0
                                    alpha:0.75];
    
    [self.lblTimerHour setTextColor:col1];
    [self.lblTimerMinute setTextColor:col1];
    [self.lblTimerSecond setTextColor:col1];
    [self.lblColonLeft setTextColor:col1];
    [self.lblColonRight setTextColor:col1];
    
    [self.timer invalidate];
    self.timer = nil;
    
    [self insertMeditationRecord];
    [[ToolBox shared] animateOut:self.btnPause delay:0.0];
    [[ToolBox shared] animateIn:self.btnDone delay:0.6];
}

-(void) timerFireMethod:(NSTimer *)timer
{
    NSTimeInterval sec;
    NSTimeInterval secElapsed;
//    NSArray *time;

    sec = [[NSDate date] timeIntervalSince1970];
    secElapsed = sec - self.secStartLast;
    
    
    if ( self.operateMode == kOpModeMeditate )
    {
        NSTimeInterval secElapsedTotal;
        secElapsedTotal = self.secDone + secElapsed;

        [self checkAlarm:secElapsedTotal];
    
        //---------------
        // Meditation finished
        if (self.alarmIndex >= [self.profile.alarms count] )
        {
            [self meditationHasFinished];
        }
        
        float secToGo = self.profile.getLastAlarmTime - secElapsedTotal;
        if (secToGo < 0)
            secToGo = 0;
        
        [self updateTimerLabels:secToGo];
    }
    else if (self.operateMode == kOpModePaused )
    {
        self.secPaused += 1;
//        self.secLastPaused += 1;
    }
}

-(void) insertMeditationRecord {
    NSInteger secPlanned;
    secPlanned = [[self.profile.alarms objectAtIndex:[self.profile.alarms count]-1] integerValue];
    
    if ( self.secDone == -1 )
        self.secDone = secPlanned;
    
    [[StorageManager sharedManager] insertTimeStart:self.secStart timePlanned:secPlanned timeDone:self.secDone];
}

-(void) leaveViewController {
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [[ToolBox shared]makeTabBarHidden:NO tabBarController:self.tabBarController];

    @try {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        UITabBarController * vc = [storyboard instantiateViewControllerWithIdentifier:@"TabVC"];
        [vc setSelectedIndex:2];

        [self presentViewController:vc animated:YES completion:nil];
    }
    @catch (NSException *e)
    {
        NSLog(@"%@", [e reason]);
    }
}

#pragma mark - Buttons actions

- (IBAction)btnStartTouch:(id)sender {
    [[ToolBox shared] animateOut:self.btnStart delay:0.0];

//    
//    [[self navigationController] setNavigationBarHidden:YES animated:NO];
//    [[ToolBox shared]makeTabBarHidden:TRUE tabBarController:self.tabBarController];
//    [self introFinished];
//    return;
    
    [self performSegueWithIdentifier:@"idSegueIntroTimer" sender:self];
}

- (IBAction)btnResumeTouch:(id)sender {
    self.operateMode = kOpModeMeditate;
    [[ToolBox shared] animateOut:self.btnResume delay:0.0];
    [[ToolBox shared] animateOut:self.btnStop delay:0.0];
    [[ToolBox shared] animateIn:self.btnPause delay:0.6];
    
//    [[ToolBox shared]delayScheduledNotification:self.secLastPaused];
//    self.secLastPaused = 0;

    self.secPaused += [[NSDate date]timeIntervalSince1970]-self.secStartLast;
    self.secStartLast = [[NSDate date]timeIntervalSince1970];
    [self scheduleNotificationsWithDelay:self.secDone];
}

- (IBAction)btnStopTouch:(id)sender {
    [self insertMeditationRecord];
    [self leaveViewController];
    [[ToolBox shared] cancelScheduleNotifications];
}

- (IBAction)btnPauseTouch:(id)sender {
    self.operateMode = kOpModePaused;
    [[ToolBox shared] animateOut:self.btnPause delay:0.0];
    [[ToolBox shared] animateIn:self.btnResume delay:0.6];
    [[ToolBox shared] animateIn:self.btnStop delay:0.6];

//    self.secLastPaused = 0;
    
    [[ToolBox shared] cancelScheduleNotifications];
    self.secDone += [[NSDate date]timeIntervalSince1970]-self.secStartLast;
    
    self.secStartLast = [[NSDate date]timeIntervalSince1970];
    
}

- (IBAction)btnDoneTouch:(id)sender {
    [self leaveViewController];
}

- (IBAction)btnCancelTouch:(id)sender {
    [self.navigationController popViewControllerAnimated:TRUE];
}

#pragma mark - Notifications

-(void) scheduleNotificationsWithDelay:(NSTimeInterval)delay
{
    // set all notification
    [[ToolBox shared] cancelScheduleNotifications];
    
    NSDate *notificationTime;
    
    for (NSNumber* sec in self.profile.alarms)
    {
        if (self.secDone >= [sec floatValue])
            continue;

        notificationTime = [NSDate dateWithTimeIntervalSinceNow:[sec floatValue]-delay];
        [[ToolBox shared] scheduleAlarmForDate:notificationTime];
    }

}

#pragma mark - IntroTimerController Delegate

-(void) introFinished
{
    
//    [[self navigationController] setNavigationBarHidden:NO animated:YES];
//    self.tabBarController.hid
//    [[ToolBox shared]makeTabBarHidden:TRUE tabBarController:self.tabBarController];

    [self scheduleNotificationsWithDelay:0];
    [[ToolBox shared] animateIn:self.btnPause delay:0.0];

    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                  target:self
                                                selector:@selector(timerFireMethod:)
                                                userInfo:nil
                                                 repeats:YES];
    self.secStart = [[NSDate date ] timeIntervalSince1970 ];
    self.secStartLast = self.secStart;
    
//    self.secLastCheckpoint = [[NSDate date] timeIntervalSince1970];
//    self.checkPoint = [NSDate date];
}



@end
