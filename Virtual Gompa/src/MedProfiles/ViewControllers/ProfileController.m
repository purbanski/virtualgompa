//
//  ProfileController.m
//  Budda Friend
//
//  Created by Przemek Urbanski on 03/01/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#import "ProfileController.h"
#import "ProfileTableCell.h"
#import "ProfileTableCellFooter.h"
#import "ToolBox.h"
#import "Settings.h"
#import "AddAlarmProfileController.h"

@interface ProfileController ()

//@property (nonatomic,weak) NSMutableArray *alarms;
@property ProfileTableCellHeader *headerCell;
@property BOOL dirty;
@property NSString *orgProfileName;

@property NSInteger addAlarmOperateMode; // AddAlarmProfileController::OperateMode
@property NSInteger alarmToSet;

@end

@implementation ProfileController

@synthesize profile;

typedef NS_ENUM(NSUInteger, MsgBoxTag) {
    kMsgBoxProfileExists = 0,
    kMsgBoxProfileChanged
};

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.headerCell = nil;

    self.tblAlarms.delegate = self;
    self.tblAlarms.dataSource = self;

    self.dirty = NO;
    
    [self reload];
    
    self.navigationItem.hidesBackButton = YES; // Important

    UIBarButtonItem *barItem;
    barItem = [UIBarButtonItem alloc];
    barItem = [barItem initWithTitle:@"Cancel"
                               style:UIBarButtonItemStylePlain
                              target:self
                              action:@selector(btnCancelTouch:)];
    [barItem setTintColor: [UIColor colorWithRed:1 green:0.5f blue:0.0f alpha:1.0f]];
    self.navigationItem.leftBarButtonItem = barItem;
    
    self.orgProfileName = self.profile.profileName;
    
//    [[ToolBox shared]makeTabBarHidden:TRUE tabBarController:self.tabBarController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self btnSaveCheck];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

-(void) viewWillDisappear:(BOOL)animated
{
    if (! self.orgProfileName || ! self.orgProfileName.length )
        return;
    
    NSComparisonResult result;
    result = [self.orgProfileName compare:self.profile.profileName];
    
    if ( result == NSOrderedSame )
        return;
    
    [[Settings shared] removeProfileForKey:self.orgProfileName];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ( [[segue identifier] isEqualToString:@"idSegueAddAlarmToProfile" ] )
    {
        AddAlarmProfileController *controler = [segue destinationViewController];
        controler.delegate = self;
        controler.seconds = [NSNumber numberWithInteger:self.alarmToSet];
        controler.operateMode = self.addAlarmOperateMode;
    }
}

#pragma mark - Add Alarm Profile delegate

-(BOOL)alarmSaved:(NSInteger)secondsCount orgSeconds:(NSInteger)orgSeconds forceSave:(BOOL)force;
{
    if (secondsCount != orgSeconds )
        [self.profile.alarms removeObject:[NSNumber numberWithInteger:orgSeconds]];
        
    NSNumber *sec;
    sec = [NSNumber numberWithInteger:secondsCount];
    
//    NSLog(@"%d %d", [self.profile.alarms indexOfObject:sec], [self.profile.alarms count]);
    if ([self.profile.alarms indexOfObject:sec] != NSNotFound && ! force)
        return FALSE;
    
    self.dirty = YES;
    
    // check needed in case of force
    // without check can result in double
    // entry of single alarm
    if ([self.profile.alarms indexOfObject:sec] == NSNotFound)
        [self.profile.alarms addObject:sec];
    
    [self reload];
    
    return TRUE;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return 1;
    return self.profile.alarms.count + 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    ProfileTableCell *cell;
    
    if ( indexPath.row == 0 )
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"ProfileTableCellHeader"];
//        cell.backgroundColor = [UIColor colorWithRed:247.0f/256.0f
//                                               green:229.0f/256.0f
//                                                blue:23.0f/256.0f
//                                               alpha:0.45];
        self.headerCell = (ProfileTableCellHeader*) cell;
        self.headerCell.delegate = self;
        if ( self.profile )
        {
            self.headerCell.txtAreaName.text = self.profile.profileName;
        }
    }
    else if ( indexPath.row == [self.profile.alarms count]+1 )
    {
        ProfileTableCellFooter *cellFooter;
        cellFooter = [tableView dequeueReusableCellWithIdentifier:@"ProfileTableCellFooter"];
        [cellFooter.btnAddAlarm addTarget:self
                                   action:@selector(btnAddAlarmTouch:)
                         forControlEvents:UIControlEventTouchUpInside];
        cell = (ProfileTableCell*) cellFooter;
    }
    else
    {
        NSInteger sec;
        NSInteger index;
        NSString *timeStr;
    
        cell = [tableView dequeueReusableCellWithIdentifier:@"ProfileTableCell"];
        index = indexPath.row - 1;
    
        sec = [[self.profile.alarms objectAtIndex:index] integerValue];
        timeStr = [[ToolBox shared]secondsToTimeStrShort:sec];
    
        cell.lblTime.text = timeStr;
        [cell.btnEditTime addTarget:self
                             action:@selector(btnEditTimeTouch:)
                   forControlEvents:UIControlEventTouchUpInside];
//        cell.backgroundColor = [UIColor colorWithRed:247.0f/256.0f
//                                               green:229.0f/256.0f
//                                                blue:23.0f/256.0f
//                                               alpha:0.25];
    }
    return cell;
}

//------------------
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0 )
        return 72;
    
    return 54;
}

//------------------
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
//        return NO;
    return YES;
}

//------------------
-  (void)tableView:(UITableView *)tableView
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
 forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        NSInteger index;
    
        index = indexPath.row - 1;

        [self.profile.alarms removeObjectAtIndex:index];
        [self.tblAlarms deleteRowsAtIndexPaths:@[indexPath]
                              withRowAnimation:UITableViewRowAnimationTop];
        [self.tblAlarms reloadData];
        self.dirty = YES;
        [self btnSaveCheck];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert)
    {
        //        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - ProfileTableCellHeader delegate

-(void) txtEditChanged:(NSString *)text
{
    self.profile.profileName = text;
    self.dirty = YES;
    
    [self btnSaveCheck];
    
//    [self.view endEditing:YES];
}

#pragma mark - Navigation buttons

- (IBAction)btnSaveTouch:(id)sender {
    
    if ( ! [self isDataEnteredValid])
        return;
    
    NSString *profileName;
    if (self.headerCell)
        profileName = self.headerCell.txtAreaName.text;
    else
        profileName = @"- recover -";
    
    self.profile.profileName = profileName;
    
    BOOL forceSave = FALSE;
    if ( [self.orgProfileName compare:profileName] == NSOrderedSame)
        forceSave = TRUE;

    if ( [self.delegate profileSaved:profileName
                              alarms:self.profile.alarms
                           forceSave:forceSave] )
        [self.navigationController popViewControllerAnimated:YES];
    else
    {
        UIAlertView *alert;
        alert = [[UIAlertView alloc]initWithTitle:@"Profile exists"
                                          message:[NSString stringWithFormat:@"Profile named %@ exists.", profileName]
                                         delegate:self
                                cancelButtonTitle:@"Overwrite"
                                otherButtonTitles:@"Cancel", nil];

        [alert setTag:kMsgBoxProfileExists];
        [alert show];
    }
}

-(void)btnCancelTouch:(UIBarButtonItem *)sender
{
    if (( ! self.dirty ) || (
                             ( self.dirty ) &&
                             ( ! [self.profile.alarms count] ) &&
                             ( ! self.profile.profileName.length )))
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
        return;
    }
    
    UIAlertView *alert;
    alert = [[UIAlertView alloc]initWithTitle:@"Profile has changed"
                                      message:@"Are you sure you want to cancel?"
                                     delegate:self
                            cancelButtonTitle:@"Yes"
                            otherButtonTitles:@"No", nil];

    [alert setTag:kMsgBoxProfileChanged];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch ([alertView tag]) {
        case kMsgBoxProfileExists:
            [self alertHandleProfileExists:buttonIndex];
            break;

        case kMsgBoxProfileChanged:
            [self alertHandleProfileChanged:buttonIndex];
            break;
            
        default:
            break;
    }
}

-(void)alertHandleProfileExists:(NSInteger)buttonIndex
 {
    switch (buttonIndex) {
        case 0: // Overwrite
            [self.delegate profileSaved:self.profile.profileName
                                 alarms:self.profile.alarms
                              forceSave:TRUE];
            [self.navigationController popViewControllerAnimated:YES];
            break;
            
        case 1: // Cancel
        default:
            break;
    }
}

-(void)alertHandleProfileChanged:(NSInteger)buttonIndex
{
    // Do you want to cancel?
    switch (buttonIndex) {
        case 0: // YES
            [self.navigationController popToRootViewControllerAnimated:TRUE];
            break;
            
        case 1: // NO
        default:
            break;
    }
}

-(void) btnSaveCheck
{
    if (!self.headerCell)
        return;
    
    NSString *profileName;
    profileName = self.headerCell.txtAreaName.text;
    
    if (profileName.length && [self.profile.alarms count] && self.dirty )
        [self.btnSave setEnabled:TRUE];
    else
        [self.btnSave setEnabled:FALSE];
}

-(void) btnEditTimeTouch:(id)sender
{
    UITableViewCell *clickedCell = (UITableViewCell *)[[sender superview] superview];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:clickedCell];
    
    self.alarmToSet = [[self.profile.alarms objectAtIndex:indexPath.row - 1] integerValue];
    self.addAlarmOperateMode = kAddAlarmProfileController_OpModeEdit;
    [self performSegueWithIdentifier:@"idSegueAddAlarmToProfile" sender: self];
}

-(void) btnAddAlarmTouch:(id)sender
{
    if ([self.profile.alarms count])
    {
        NSInteger alarmTime;
        NSArray *alarms;
        alarms = self.profile.alarms;
        alarmTime = [[alarms objectAtIndex:[alarms count]-1] integerValue];
        self.alarmToSet = alarmTime + 5 * 60;
    }
    else
        self.alarmToSet = 3600;
    
    self.addAlarmOperateMode = kAddAlarmProfileController_OpModeAddNew;
    [self performSegueWithIdentifier:@"idSegueAddAlarmToProfile" sender: self];
}

#pragma mark - Other

-(void) reload
{
    NSArray *arr = [ self.profile.alarms sortedArrayUsingSelector: @selector(compare:)];
    self.profile.alarms = [[NSMutableArray alloc]initWithArray:arr];
    
    [self.tblAlarms reloadData];
}

-(BOOL)isDataEnteredValid {
    if ( ! self.profile.profileName.length )
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No profile name"
                                                        message:@"You must set profile name."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return FALSE;
    }

    if ( ! [self.profile.alarms count])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No alarm"
                                                        message:@"You must set at least one alarm."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return FALSE;
    }
    
    // profile with this name
    return TRUE;
}



@end
