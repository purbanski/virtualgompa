//
//  ProfilesController.m
//  Budda Friend
//
//  Created by Przemek Urbanski on 03/01/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#import "ProfilesController.h"
#import "ProfilesTableCell.h"
#import "ProfileController.h"
#import "MeditateProfileController.h"
#import "Settings.h"
#import "Profile.h"
#import "ToolBox.h"
#import "AudioManager.h"

@interface ProfilesController ()

@property NSMutableDictionary *profiles;
@property (nonatomic) NSInteger selectedRecordId;

@end

@implementation ProfilesController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.selectedRecordId = -1;

    self.tblProfiles.delegate = self;
    self.tblProfiles.dataSource = self;
}

- (void) viewWillAppear:(BOOL)animated
{
    [[ToolBox shared]setTabBarTint:self.tabBarController];
    [self reload];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    //-----------------
    // Edit record
    if ( [[segue identifier] isEqualToString:@"idSegueEditProfile"] )
    {
        ProfileController *controller;
        controller = [segue destinationViewController];
        controller.delegate = self;
        
        if ( self.selectedRecordId == -1 )
        {
            Profile *p;
            p = [Profile createWithName:@"" alarms:@[]];
            [controller setProfile:p];
        }
        else
        {
            Profile *p;
            NSArray *keys;
            NSString *profileName;
            NSArray *alarms;
            
            keys = [self.profiles allKeys];
            keys = [keys sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
            profileName = [keys objectAtIndex:self.selectedRecordId];
            alarms = [self.profiles objectForKey:profileName];
            
            p = [Profile createWithName:profileName alarms:alarms];
            [controller setProfile:p];
        }
    }
    
    
    //------------------------
    // Meditate profile record

    if ( [[segue identifier] isEqualToString:@"idSegueMeditateProfile"] && self.selectedRecordId != -1 )
    {
        MeditateProfileController *controller;
        controller = [segue destinationViewController];
//        controller.delegate = self;
        
        Profile *p;
        NSArray *keys;
        NSString *profileName;
        NSArray *alarms;
        
        keys = [self.profiles allKeys];
        keys = [keys sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
        profileName = [keys objectAtIndex:self.selectedRecordId];
        alarms = [self.profiles objectForKey:profileName];
        
        p = [Profile createWithName:profileName alarms:alarms];
        [controller setProfile:p];
    }

    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.profiles count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ProfilesTableCell *cell;

    NSArray *keys = [self.profiles allKeys];
    NSString *profileName;
    NSArray  *profileAlarms;
        
    
    keys = [keys sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    profileName = [keys objectAtIndex:indexPath.row];
//        NSLog(@"%@",profileName);
    profileAlarms = [self.profiles objectForKey:profileName];

    cell = (ProfilesTableCell*)[tableView dequeueReusableCellWithIdentifier:@"ProfilesTableCell"];
//    cell.backgroundColor = [UIColor colorWithRed:247.0f/256.0f
//                                           green:229.0f/256.0f
//                                            blue:23.0f/256.0f
//                                           alpha:0.25];
    NSArray *alarms;
    NSString *lastAlarm;
    NSNumber *lastAlarmSec;
    
    alarms = [profileAlarms sortedArrayUsingSelector:@selector(compare:)];
    
    if ( [alarms count])
    {
        lastAlarmSec = [alarms objectAtIndex:([alarms count] - 1)];
        lastAlarm = [[ToolBox shared] secondsToTimeStrShort:[lastAlarmSec integerValue]];
        cell.lblTime.text = lastAlarm;
    }
    else
    {
        cell.lblTime.text = @"00:00";
    }
    
    cell.lblProfileName.text = profileName;
    cell.lblAlarmCount.text = [NSString stringWithFormat:@"%d", [profileAlarms count]];
    
    [cell.btnEdit addTarget:self
                     action:@selector(btnEditTouch:)
           forControlEvents:UIControlEventTouchUpInside];

    [cell.btnMeditate addTarget:self
                         action:@selector(btnMEditateTouch:)
               forControlEvents:UIControlEventTouchUpInside];

    [cell.btnMeditate setTitle: profileName forState: UIControlStateSelected | UIControlStateHighlighted];
    return cell;
}

//------------------
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 84;
}

//------------------
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    //        return NO;
    return YES;
}

//------------------
-  (void)tableView:(UITableView *)tableView
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
 forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSArray *keys;
        NSString *profileName;
        
        keys = [self.profiles allKeys];
        keys = [keys sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
        profileName = [keys objectAtIndex:indexPath.row];
        
        [self.profiles removeObjectForKey:profileName];
        [self.tblProfiles deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tblProfiles reloadData];
        
        [[Settings shared]setProfiles:self.profiles];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert)
    {
        //        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Delegate ProfileController

-(BOOL) profileSaved:(NSString *)profileName alarms:(NSArray *)alarms forceSave:(BOOL)force{

    // Check if profile already exists
    id obj = [self.profiles objectForKey:profileName];
    if (obj && !force)
        return FALSE;
    
    [self.profiles setObject:alarms forKey:profileName];
    [[Settings shared]setProfiles:self.profiles];
    [self.tblProfiles reloadData];

//    NSLog(@"%d",[self.profiles count]);
    return TRUE;
}

#pragma mark - Button method (table cells)

-(IBAction)btnAddNewProfileTouch:(id)sender {
    self.selectedRecordId = -1;
    [self performSegueWithIdentifier:@"idSegueEditProfile" sender:self];
    [[AudioManager shared]playSound:eSoundTap];
}

-(void)btnEditTouch:(id)sender {
    UITableViewCell *clickedCell = (UITableViewCell *)[[sender superview] superview];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:clickedCell];
    
    self.selectedRecordId = indexPath.row;
    [self performSegueWithIdentifier:@"idSegueEditProfile" sender:self];
    [[AudioManager shared]playSound:eSoundTap];
}

-(void)btnMEditateTouch:(id)sender {
    UITableViewCell *clickedCell = (UITableViewCell *)[[sender superview] superview];
    NSIndexPath *indexPath = [self.tableView indexPathForCell:clickedCell];
    
    self.selectedRecordId = indexPath.row;
    [self performSegueWithIdentifier:@"idSegueMeditateProfile" sender:self];
    [[AudioManager shared]playSound:eSoundTap];
}


#pragma mark - Other
-(void) reload {
    self.profiles = [NSMutableDictionary dictionaryWithDictionary:[[Settings shared]getProfiles]];
    self.selectedRecordId = -1;
}

-(NSArray*) getProfileForIndexPath:(NSIndexPath*)indexPath
{
    NSArray *keys = [self.profiles allKeys];
    NSArray  *profiles;
    NSString *profileName;
    
    keys = [keys sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    profileName = [keys objectAtIndex:indexPath.row];
   
    profiles = [self.profiles objectForKey:profileName];
    return profiles;
}

//-(BOOL)shouldAutorotate;
//-(NSUInteger)supportedInterfaceOrientations;

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
//    return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
//}
-(BOOL)shouldAutorotate {
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

//- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
//    return UIInterfaceOrientationPortrait;
//}
@end
