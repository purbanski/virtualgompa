//
//  ProfilesTableCell.h
//  Budda Friend
//
//  Created by Przemek Urbanski on 03/01/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfilesTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblProfileName;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblAlarmCount;
@property (weak, nonatomic) IBOutlet UIButton *btnMeditate;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;

@end
