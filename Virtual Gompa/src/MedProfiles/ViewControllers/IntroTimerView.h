//
//  IntroTimerView.h
//  Budda Friend
//
//  Created by Przemek Urbanski on 07/01/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroTimerView : UIView
@property (weak, nonatomic) IBOutlet UILabel *lblTimer;

@end
