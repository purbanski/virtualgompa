//
//  ProfileTableCellFooter.h
//  Budda Friend
//
//  Created by Przemek Urbanski on 03/01/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileTableCellFooter : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btnAddAlarm;

@end
