//
//  AddAlarmProfileController.m
//  Budda Friend
//
//  Created by Przemek Urbanski on 03/01/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#import "AddAlarmProfileController.h"
#import "ToolBox.h"

@interface AddAlarmProfileController ()

@end

@implementation AddAlarmProfileController
@synthesize operateMode;
@synthesize seconds;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSArray *time;
    time = [[ToolBox shared]secondsToTime:[self.seconds integerValue]];
    
    NSDate *result;
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSInteger minutesToSet;
    NSInteger hoursToSet;
    
    minutesToSet = [[time objectAtIndex:1]integerValue];
    hoursToSet = [[time objectAtIndex:2]integerValue];
    
    [comps setMinute:minutesToSet];
    [comps setHour:hoursToSet];
    
    result = [gregorian dateFromComponents:comps];

    [self.pickerTime setDate:result];
    self.navigationItem.hidesBackButton = YES;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnSaveTouch:(id)sender {
    NSInteger sec;
    NSDate *timerDate;
    NSDictionary *dateDic;
    
    timerDate = [[self pickerTime] date];
    dateDic = [[ToolBox shared] dateToDictionary:timerDate];
    
    sec  = [[dateDic valueForKey:@"hour"  ] integerValue] * 60 * 60 ;
    sec += [[dateDic valueForKey:@"minute"] integerValue] * 60;
    
    // debug
    // remove me
//    sec /= 60;
    if ( self.operateMode == kAddAlarmProfileController_OpModeAddNew)
    {
        [self.delegate alarmSaved:sec orgSeconds:sec forceSave:TRUE];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self.delegate alarmSaved:sec orgSeconds:[self.seconds integerValue] forceSave:TRUE];
        [self.navigationController popViewControllerAnimated:YES];
    }
//    else
//    {
//        UIAlertView *alert;
//        NSString *msg;
//        NSString *timeStr;
//        
//        timeStr = [[ToolBox shared]secondsToTimeStrShort:sec];
//        msg = @"Please change time to continue.";
//        
//        alert = [[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"Alarm for %@ is already set", timeStr]
//                                          message:msg
//                                         delegate:nil
//                                cancelButtonTitle:@"OK"
//                                otherButtonTitles:nil];
//        [alert show];
//    }
}
- (IBAction)btnCancelTouch:(id)sender {
    [self.navigationController popViewControllerAnimated:TRUE];
}

@end
