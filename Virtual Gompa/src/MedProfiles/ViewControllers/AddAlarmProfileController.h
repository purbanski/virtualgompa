//
//  AddAlarmProfileController.h
//  Budda Friend
//
//  Created by Przemek Urbanski on 03/01/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddAlarmProfileControllerDelegate

-(BOOL)alarmSaved:(NSInteger)secondsCount orgSeconds:(NSInteger)orgSeconds forceSave:(BOOL)force;

@end

typedef NS_ENUM(NSUInteger, AddAlarmProfileControllerOperateMode) {
    kAddAlarmProfileController_OpModeAddNew = 0,
    kAddAlarmProfileController_OpModeEdit
};
@interface AddAlarmProfileController : UIViewController

@property (nonatomic, strong) id<AddAlarmProfileControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIDatePicker *pickerTime;

@property (strong,nonatomic) NSNumber* seconds;
@property AddAlarmProfileControllerOperateMode operateMode;

- (IBAction)btnSaveTouch:(id)sender;
- (IBAction)btnCancelTouch:(id)sender;

@end
