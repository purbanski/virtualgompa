//
//  ProfileController.h
//  Budda Friend
//
//  Created by Przemek Urbanski on 03/01/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddAlarmProfileController.h"
#import "Profile.h"
#import "ProfileTableCellHeader.h"

@protocol ProfileControllerDelegate

-(BOOL)profileSaved:(NSString*)profileName alarms:(NSArray*)alarms forceSave:(BOOL)force;

@end

@interface ProfileController : UITableViewController<
    AddAlarmProfileControllerDelegate,
    ProfileTableCellHeaderDelegate,
    UIAlertViewDelegate>

@property (nonatomic,strong) IBOutlet UITableView *tblAlarms;
@property (nonatomic,weak)   id<ProfileControllerDelegate> delegate;
@property Profile *profile;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnSave;

- (IBAction)btnSaveTouch:(id)sender;

@end
