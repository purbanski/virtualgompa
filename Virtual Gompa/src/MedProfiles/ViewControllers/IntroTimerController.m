    //
//  IntroTimerController.m
//  Budda Friend
//
//  Created by Przemek Urbanski on 07/01/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#import "IntroTimerController.h"
#import "ToolBox.h"
#import "AudioManager.h"

@interface IntroTimerController ()

@end

@implementation IntroTimerController
@synthesize seconds;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.lblTimer.text = @"";
    
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    [[ToolBox shared]makeTabBarHidden:TRUE tabBarController:self.tabBarController];
}


-(void) animCountDown {
    [UIView animateWithDuration:1.0
                          delay:0.0
                        options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                     animations:^(void)
     {
         [self.lblTimer setAlpha:0.0];
     }
                     completion:^(BOOL finished)
     {
         if(finished)
         {
             NSArray *hit = @[@1,@2,@3,@9];
             self.seconds--;
             if ( [hit containsObject:[NSNumber numberWithInteger:self.seconds]] )
                 [[AudioManager shared]playSound:eSoundTap];
             
             
             if (self.seconds>0)
             {
                 self.lblTimer.text = [NSString stringWithFormat:@"%d", self.seconds ];
                 [self.lblTimer setAlpha:1.0];

                 [self animCountDown];
             }
             else
             {
//                 [self.navigationController popViewControllerAnimated:TRUE];
//                 [self]
//                 [self.navigationController popToRootViewControllerAnimated:TRUE];
                 [self.navigationController popToViewController:self.delegate animated:TRUE];
                 
                 [self.delegate introFinished];
             }
         }
     }];

}

- (void)viewDidAppear:(BOOL)animated {
    self.lblTimer.text = [NSString stringWithFormat:@"%d",self.seconds];
    [self animCountDown];
    [[AudioManager shared]playSound:eSoundTap];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Timer
//-(void) timerFireMethod:(id)sender
//{
//    self.lblTimer.text = [NSString stringWithFormat:@"%d", self.sec ];
//    
//    if (!self.sec)
//    {
//        [self.timer invalidate];
//        [self.navigationController popViewControllerAnimated:TRUE];
//        return;
//    }
//    self.sec--;
//}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
