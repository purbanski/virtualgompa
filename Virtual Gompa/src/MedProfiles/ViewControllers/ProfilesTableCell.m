//
//  ProfilesTableCell.m
//  Budda Friend
//
//  Created by Przemek Urbanski on 03/01/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#import "ProfilesTableCell.h"

@implementation ProfilesTableCell

- (void)awakeFromNib {
    // Initialization code
    
    UIView *myBackView = [[UIView alloc] initWithFrame:self.frame];
    myBackView.backgroundColor = [UIColor colorWithRed:1 green:0.8 blue:0.6 alpha:1];
    self.selectedBackgroundView = myBackView;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected)
        [self.btnMeditate sendActionsForControlEvents:UIControlEventTouchUpInside];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    

    NSString *title;
    NSString *profileName;
    NSInteger maxLen;
    maxLen = 16;
    
    profileName = self.lblProfileName.text;
    
    if (profileName.length > maxLen)
    {
        title = [profileName substringWithRange:NSMakeRange(0, maxLen)];
        title = [NSString stringWithFormat:@"%@...", title ];
    }
    else
        title = profileName;
    
    [self.btnMeditate setTitle:title forState:UIControlStateNormal];
    self.btnMeditate.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
}

@end
