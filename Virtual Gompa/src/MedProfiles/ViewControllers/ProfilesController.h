//
//  ProfilesController.h
//  Budda Friend
//
//  Created by Przemek Urbanski on 03/01/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileController.h"

@interface ProfilesController : UITableViewController<ProfileControllerDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tblProfiles;

- (IBAction)btnAddNewProfileTouch:(id)sender;

@end
