//
//  MeditateProfileController.h
//  Budda Friend
//
//  Created by Przemek Urbanski on 04/01/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Profile.h"
#import "IntroTimerController.h"

//@protocol MeditateProfileControllerDelegate
//-(void)profileSaved:(NSString*)profileName alarms:(NSArray*)alarms;
//@end

@interface MeditateProfileController : UIViewController<IntroTimerControllerDelegate>

@property Profile *profile;

@property (weak, nonatomic) IBOutlet UILabel *lblProfileName;
@property (weak, nonatomic) IBOutlet UILabel *lblTimerHour;
@property (weak, nonatomic) IBOutlet UILabel *lblTimerMinute;
@property (weak, nonatomic) IBOutlet UILabel *lblTimerSecond;
@property (weak, nonatomic) IBOutlet UILabel *lblColonLeft;
@property (weak, nonatomic) IBOutlet UILabel *lblColonRight;

@property (weak, nonatomic) IBOutlet UIButton *btnStart;
@property (weak, nonatomic) IBOutlet UIButton *btnPause;
@property (weak, nonatomic) IBOutlet UIButton *btnStop;
@property (weak, nonatomic) IBOutlet UIButton *btnResume;
@property (weak, nonatomic) IBOutlet UIButton *btnDone;

- (IBAction)btnStartTouch:(id)sender;
- (IBAction)btnResumeTouch:(id)sender;
- (IBAction)btnStopTouch:(id)sender;
- (IBAction)btnPauseTouch:(id)sender;
- (IBAction)btnDoneTouch:(id)sender;
- (IBAction)btnCancelTouch:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *lblAlarmCount;
@property (weak, nonatomic) IBOutlet UILabel *lblNextAlarmHour;
@property (weak, nonatomic) IBOutlet UILabel *lblNextAlarmMinute;
@property (weak, nonatomic) IBOutlet UILabel *lblNextAlarmSecond;

@property (weak, nonatomic) IBOutlet UIView *viewAlarmPanel;
@property (weak, nonatomic) IBOutlet UIView *viewIntroTimer;

@end





