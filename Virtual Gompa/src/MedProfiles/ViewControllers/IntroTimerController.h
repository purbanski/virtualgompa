//
//  IntroTimerController.h
//  Budda Friend
//
//  Created by Przemek Urbanski on 07/01/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IntroTimerControllerDelegate
-(void) introFinished;
@end

@interface IntroTimerController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblTimer;
@property (strong,nonatomic) id<IntroTimerControllerDelegate> delegate;
@property NSInteger seconds;

@end
