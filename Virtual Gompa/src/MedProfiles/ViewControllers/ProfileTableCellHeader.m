//
//  ProfileTableCellHeader.m
//  Budda Friend
//
//  Created by Przemek Urbanski on 03/01/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#import "ProfileTableCellHeader.h"

@implementation ProfileTableCellHeader

- (void)awakeFromNib {
    // Initialization code
    self.delegate = nil;
    
    [self.txtAreaName addTarget:self.txtAreaName
                         action:@selector(resignFirstResponder)
               forControlEvents:UIControlEventEditingDidEndOnExit];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:NO animated:NO];

}

- (IBAction)txtAreaNamaTouch:(id)sender {
//    NSLog(@"%@", self.txtAreaName.text);
}

- (IBAction)txtAreaNamaEditChanged:(id)sender {
    if (self.delegate)
        [self.delegate txtEditChanged:self.txtAreaName.text];
//    NSLog(@"%@ edit changed", self.txtAreaName.text);
}

- (IBAction)txtAreaNamaEditBegin:(id)sender {
//    NSLog(@"%@ edit begin", self.txtAreaName.text);
}

- (IBAction)txtAreaNamaEditEnd:(id)sender {
//    NSLog(@"%@ eidt end", self.txtAreaName.text);
}

- (IBAction)txtAreaNamaValueChanged:(id)sender {
//    NSLog(@"%@ Value changed", self.txtAreaName.text);
}

- (IBAction)txtAreaNameTouchCancel:(id)sender {
//    NSLog(@"Touch cancel");
}

- (IBAction)txtAreaNameTouchDown:(id)sender {
//    NSLog(@"touch down");
}


@end
