//
//  ProfileTableCellHeader.h
//  Budda Friend
//
//  Created by Przemek Urbanski on 03/01/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ProfileTableCellHeaderDelegate

-(void) txtEditChanged:(NSString*) text;

@end

@interface ProfileTableCellHeader : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *txtAreaName;
@property (weak, nonatomic) id<ProfileTableCellHeaderDelegate> delegate;

- (IBAction)txtAreaNamaEditChanged:(id)sender;
- (IBAction)txtAreaNamaEditBegin:(id)sender;
- (IBAction)txtAreaNamaEditEnd:(id)sender;
- (IBAction)txtAreaNamaValueChanged:(id)sender;
- (IBAction)txtAreaNameTouchCancel:(id)sender;
- (IBAction)txtAreaNameTouchDown:(id)sender;
@end
