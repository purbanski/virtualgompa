//
//  Profile.h
//  Budda Friend
//
//  Created by Przemek Urbanski on 03/01/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Profile : NSObject

@property (nonatomic,strong) NSString* profileName;
@property (nonatomic,strong) NSMutableArray* alarms;


+(id) createWithName:(NSString*)profileName
              alarms:(NSArray*)alarms;

//+(id) createWithIndex:(NSInteger)index;

-(float) getLastAlarmTime;
-(NSString*) getLastAlarmTimeStr;

@end
