//
//  FirstViewController.h
//  Budda Friend
//
//  Created by Przemek Urbanski on 24/11/14.
//  Copyright (c) 2014 Przemek Urbanski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StatsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *labelStats24Hours;
@property (weak, nonatomic) IBOutlet UILabel *labelStats3Days;
@property (weak, nonatomic) IBOutlet UILabel *labelStats7Days;
@property (weak, nonatomic) IBOutlet UILabel *labelStats14Days;
@property (weak, nonatomic) IBOutlet UILabel *labelStats30Days;

@end

