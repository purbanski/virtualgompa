//
//  SecondViewController.h
//  Budda Friend
//
//  Created by Przemek Urbanski on 24/11/14.
//  Copyright (c) 2014 Przemek Urbanski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IntroTimerController.h"

@interface MeditateViewController : UIViewController< IntroTimerControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (weak, nonatomic) IBOutlet UIButton *stopButton;

@property (weak, nonatomic) IBOutlet UIButton *testSoundButton;

@property (weak, nonatomic) IBOutlet UIView *timersView;

@property (weak, nonatomic) IBOutlet UILabel *labelHourElapsed;
@property (weak, nonatomic) IBOutlet UILabel *labelMinuteElapsed;
@property (weak, nonatomic) IBOutlet UILabel *labelSecondElapsed;
@property (weak, nonatomic) IBOutlet UILabel *labelColon1Elapsed;
@property (weak, nonatomic) IBOutlet UILabel *labelColon2Elapsed;
@property (weak, nonatomic) IBOutlet UILabel *labelDescElapsed;

@property (weak, nonatomic) IBOutlet UILabel *labelHourToGo;
@property (weak, nonatomic) IBOutlet UILabel *labelMinuteToGo;
@property (weak, nonatomic) IBOutlet UILabel *labelSecondToGo;
@property (weak, nonatomic) IBOutlet UILabel *labelColon1ToGo;
@property (weak, nonatomic) IBOutlet UILabel *labelColon2ToGo;
@property (weak, nonatomic) IBOutlet UILabel *labelDescToGo;

@property (weak, nonatomic) IBOutlet UIDatePicker *timeSetter;

@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;

@end

