//
//  DebugTableController.h
//  Budda Friend
//
//  Created by Przemek Urbanski on 07/01/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DebugTableController : UITableViewController

@property (strong, nonatomic) IBOutlet UITableView *tblMeditate;

@end
