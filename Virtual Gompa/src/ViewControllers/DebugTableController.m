//
//  DebugTableController.m
//  Budda Friend
//
//  Created by Przemek Urbanski on 07/01/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#import "DebugTableController.h"
#import "DebugTableCell.h"
#import "AudioManager.h"
#import "ToolBox.h"
#import "StorageManager.h"

@interface DebugTableController ()

@property NSArray* records;

@end

@implementation DebugTableController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.tblMeditate.delegate = self;
    self.tblMeditate.dataSource = self;
    
    [self reload];
}

- (void)viewWillAppear:(BOOL)animated {
    [[AudioManager shared]playSound:eSoundTap];
    [self reload];
    [self.tblMeditate reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.records count];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdent = @"DebugTableCell";
    
    DebugTableCell *cell;
    cell = (DebugTableCell *)[tableView dequeueReusableCellWithIdentifier:cellIdent];
    
//    if (cell == nil)
//    {
//        NSArray *nib;
//        nib = [[NSBundle mainBundle] loadNibNamed:simpleTableIdentifier owner:self options:nil];
//        cell = [nib objectAtIndex:0];
//    }
    
    if ( ! [self.records count] )
        return cell;
    
    NSInteger ind = indexPath.row;
    NSManagedObject *r;

    r = [self.records objectAtIndex:ind] ;

    NSInteger timeStart     = [[r valueForKey:@"timeStart"] integerValue];
    NSInteger timeDone      = [[r valueForKey:@"timeDone"] integerValue];
    NSInteger timePlanned   = [[r valueForKey:@"timePlanned"] integerValue];
    
    cell.lblMedStartDate.text    = [ NSString stringWithFormat:@"%@",
                                   [[ToolBox shared] secondsToDateStr:timeStart ]];
    NSString *tmStart;
    tmStart = [ NSString stringWithFormat:@"%@", [[ToolBox shared] secondsToTimeStr:timeStart]];
//    tmStart = [tmStart substringWithRange:NSMakeRange(0, tmStart.length-3)];
    cell.lblMedStartTime.text     = tmStart;
    
    NSString *tmDone;
    tmDone = [ NSString stringWithFormat:@"%@", [[ToolBox shared] secondsToTimeStr:timeDone]];
    cell.lblMedTimeDone.text     = tmDone;
    
    NSString *tmPlanned;
    tmPlanned = [ NSString stringWithFormat:@"%@", [[ToolBox shared] secondsToTimeStr:timePlanned]];
    cell.lblTimePlanned.text     = tmPlanned;

    cell.lblIndex.text = [NSString stringWithFormat:@"%d", indexPath.row+1];

    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0;
}

-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return nil;
    //    NSArray *ret = @[@"A",@"B",@"E",
    //                     @"F",@"G",@"H",
    //                     @"I",@"J",@"K",
    //                     @"L",@"M",@"N"];
    //    return ret;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
    //    return NO;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [[StorageManager sharedManager] deleteRecord:indexPath.row];
        [self reload];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}

#pragma mark - Private
-(void) reload
{
    self.records = [[StorageManager sharedManager]fetchData];
}
/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
