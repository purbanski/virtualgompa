//
//  AddRecordViewController.m
//  Budda Friend
//
//  Created by Przemek Urbanski on 29/12/14.
//  Copyright (c) 2014 Przemek Urbanski. All rights reserved.
//

#import "AddRecordViewController.h"
#import "ToolBox.h"
#import "StorageManager.h"
#import "AudioManager.h"
#import "AppDelegate.h"

@interface AddRecordViewController ()

@end

@implementation AddRecordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSDate* result;
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    [comps setMinute:0];
    [comps setHour:1];
    result = [gregorian dateFromComponents:comps];
    
    [self.pickerMedLen setDate:result];
}

- (void)viewWillAppear:(BOOL)animated {
    [[AudioManager shared]playSound:eSoundTap];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate setOrientationMask:UIInterfaceOrientationMaskPortrait];
}

- (void)viewWillDisappear:(BOOL)animated {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate setOrientationMask:UIInterfaceOrientationMaskAll];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)saveClicked:(id)sender {
    NSDate *medDate;
    NSDate *medLen;
    
    medLen = self.pickerMedLen.date;
    medDate = self.pickerMedDate.date;
    
//    NSDate *timerDate = [[self timeSetter] date];
    NSDictionary *timeDone = [[ToolBox shared] dateToDictionary:medLen];
    
    NSInteger secDone;
    NSInteger dateOfMedSec;
    
    secDone  = [[timeDone valueForKey:@"hour"  ] integerValue] * 60 * 60 ;
    secDone += [[timeDone valueForKey:@"minute"] integerValue] * 60;
    
    dateOfMedSec = [medDate timeIntervalSince1970];
    [[StorageManager sharedManager] insert:dateOfMedSec :0 :secDone ];

    [self.delegate recordSaved];
    [self.navigationController popViewControllerAnimated:YES];
}

//--------------------------
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
//    return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
//}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
        return YES;
}

-(BOOL)shouldAutorotate {
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}


- (IBAction)btnCancelTouch:(id)sender {
    [self.navigationController popViewControllerAnimated:TRUE];
}
@end
