//
//  MedFinishViewController.m
//  Budda Friend
//
//  Created by Przemek Urbanski on 30/11/14.
//  Copyright (c) 2014 Przemek Urbanski. All rights reserved.
//

#import "MedFinishViewController.h"

@interface MedFinishViewController ()

@end

@implementation MedFinishViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    UIViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"StatsVC"];
    [self presentViewController:vc animated:YES completion:nil];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
