//
//  SecondViewController.m
//  Budda Friend
//
//  Created by Przemek Urbanski on 24/11/14.
//  Copyright (c) 2014 Przemek Urbanski. All rights reserved.
//

#import "MeditateViewController.h"
#import "ToolBox.h"
#import "StorageManager.h"
#import "AudioManager.h"
#import "IntroTimerController.h"

@interface MeditateViewController ()
    @property NSUInteger timerCount;
    @property NSUInteger secStart;
    @property NSUInteger secElapsed;
    @property NSUInteger secSet;

    @property NSDate *medStartTime;
    @property bool started;
    @property bool finishSndPlayed;

    @property NSTimer *timer;

@end

@implementation MeditateViewController

@synthesize managedObjectContext;

- (id)init {
    return self;
}

- (void)viewDidLoad {

    NSDate* result;
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];

    [comps setMinute:0];
    [comps setHour:1];
    result = [gregorian dateFromComponents:comps];
   
    
    self.timerCount = 1;
    [self.timeSetter setDate:result];
    self.started = false;
    self.secSet = 60 * 60;
    self.finishSndPlayed = false;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewWillAppear:(BOOL)animated {
    [[AudioManager shared]playSound:eSoundTap];
    [[ToolBox shared]setTabBarTint:self.tabBarController];

 
    if (self.started)
        return;
    
    if (  self.interfaceOrientation == UIDeviceOrientationPortrait ||  self.interfaceOrientation == UIDeviceOrientationPortraitUpsideDown)
    {
        [[self navigationController] setNavigationBarHidden:NO animated:NO];

//        if ( [self.testSoundButton isHidden] )
            [self.testSoundButton setHidden:NO];
        [self.testSoundButton setAlpha:1];
    }
    else
    {
        [[self navigationController] setNavigationBarHidden:YES animated:NO];

        if ( ![self.testSoundButton isHidden] )
            [self.testSoundButton setHidden:TRUE];

    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)timerFireMethod:(NSTimer *)timer
{
    NSInteger sec, secToGo;
    NSArray *time;

    sec = [[NSDate date] timeIntervalSince1970];
    sec = sec - self.secStart;
    secToGo = self.secSet - sec;
    self.secElapsed = sec;
    
    
    if (secToGo <= 0 ) {
        if ( ! self.finishSndPlayed )
        {
            self.finishSndPlayed = true;
            
            [[ToolBox shared]cancelScheduleNotifications];
            
            if ( secToGo == 0 )
                [[AudioManager shared] playSound:eSoundMedFinish];
            
            UIColor *col1 = [UIColor colorWithRed:98.0/255.0 green:186.0/255.0 blue:32.0/255.0 alpha:0.75];
            UIColor *col2 = [UIColor colorWithRed:22.0/255.0 green:126.0/255.0 blue:251.0/255.0 alpha:0.30];
            
            [self.labelHourElapsed setTextColor:col1];
            [self.labelMinuteElapsed setTextColor:col1];
            [self.labelSecondElapsed setTextColor:col1];
            [self.labelColon1Elapsed setTextColor:col1];
            [self.labelColon2Elapsed setTextColor:col1];

            [self.labelHourToGo setTextColor:col2];
            [self.labelMinuteToGo setTextColor:col2];
            [self.labelSecondToGo setTextColor:col2];
            [self.labelColon1ToGo setTextColor:col2];
            [self.labelColon2ToGo setTextColor:col2];
        }
        secToGo = 0;
        
        if ( self.timer )
        {
            [self.timer invalidate];
            self.timer = nil;
        }
    }
    
    time = [[ToolBox shared] secondsToTime:secToGo];
    
    NSString *secStr = [[ToolBox shared] addLeadingZero:time[0] ];
    NSString *minStr = [[ToolBox shared] addLeadingZero:time[1] ];
    NSString *hourStr = [[ToolBox shared] addLeadingZero:time[2] ];

    [self.labelSecondToGo setText:secStr];
    [self.labelMinuteToGo setText:minStr];
    [self.labelHourToGo setText:hourStr];
    
    time = [[ToolBox shared] secondsToTime:self.secElapsed];

    secStr = [[ToolBox shared] addLeadingZero:time[0] ];
    minStr = [[ToolBox shared] addLeadingZero:time[1] ];
    hourStr = [[ToolBox shared] addLeadingZero:time[2] ];
    
    [self.labelSecondElapsed setText:secStr];
    [self.labelMinuteElapsed setText:minStr];
    [self.labelHourElapsed setText:hourStr];
}


- (IBAction)meditateStart:(id)sender {
    NSLog(@"Meditate start");
    [[AudioManager shared]playSound:eSoundTap];
    [[ToolBox shared] animateOut:self.startButton delay:0];
    [[ToolBox shared] animateOut:self.testSoundButton delay:0];
    [[ToolBox shared] animateOut:self.timeSetter delay:0];

    self.started = true;
    [self performSegueWithIdentifier:@"idSegueIntroTimerExpress" sender:self];
}

-(void) introFinished {
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                  target:self
                                                selector:@selector(timerFireMethod:)
                                                userInfo:nil
                                                 repeats:YES];

    self.secStart = [[NSDate date ] timeIntervalSince1970 ];

    NSDate *timerDate = [[self timeSetter] date];
    NSDictionary *date = [[ToolBox shared] dateToDictionary:timerDate];

    self.secSet  = [[date valueForKey:@"hour"  ] integerValue] * 60 * 60 ;
    self.secSet += [[date valueForKey:@"minute"] integerValue] * 60;

//        self.secSet = 4;

    NSDate *notificationTime;
    notificationTime = [NSDate dateWithTimeIntervalSinceNow:self.secSet];

    [[ToolBox shared]scheduleAlarmForDate:notificationTime];
    
    NSInteger sec;
    NSArray *time;
        
    sec = [[NSDate date] timeIntervalSince1970];
    sec = sec - self.secStart;
    time = [[ToolBox shared] secondsToTime:sec];
        
    NSString *minStr = [NSString stringWithFormat:@"%02i", [[date valueForKey:@"minute"] integerValue ]];
    NSString *hourStr = [NSString stringWithFormat:@"%02i", [[date valueForKey:@"hour"] integerValue]];
        
    [self.labelSecondToGo setText:@"00"];
    [self.labelMinuteToGo setText:minStr];
    [self.labelHourToGo setText:hourStr];

    [self.timersView setHidden:false];

//    [self animateOut];
    [self animeteInTimers];

//        [self.stopButton setHidden:TRUE];
    [[ToolBox shared] animateIn:self.stopButton delay:0.8];
}

- (IBAction)meditateStop:(id)sender {
    [[AudioManager shared]playSound:eSoundTap];
    
    if ( self.timer )
    {
        [self.timer invalidate];
        self.timer = nil;
    }

    [[ToolBox shared]cancelScheduleNotifications];

    
//    NSInteger secElapsed = [[NSDate date] timeIntervalSince1970] - self.secStart;
    [[StorageManager sharedManager] insert:self.secStart :self.secSet :self.secElapsed ];
    @try {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        UITabBarController * vc = [storyboard instantiateViewControllerWithIdentifier:@"TabVC"];
        [vc setSelectedIndex:2];
        
        [self presentViewController:vc animated:YES completion:nil];
    }
    @catch (NSException *e)
    {
        NSLog(@"%@", [e reason]);
    }
}

-(void) animeteInTimers {
    [[ToolBox shared] animateIn:self.labelHourElapsed];
    [[ToolBox shared] animateIn:self.labelMinuteElapsed];
    [[ToolBox shared] animateIn:self.labelSecondElapsed];
    [[ToolBox shared] animateIn:self.labelColon1Elapsed];
    [[ToolBox shared] animateIn:self.labelColon2Elapsed];
    [[ToolBox shared] animateIn:self.labelDescElapsed];

    [[ToolBox shared] animateIn:self.labelHourToGo];
    [[ToolBox shared] animateIn:self.labelMinuteToGo];
    [[ToolBox shared] animateIn:self.labelSecondToGo];
    [[ToolBox shared] animateIn:self.labelColon1ToGo];
    [[ToolBox shared] animateIn:self.labelColon2ToGo];
    [[ToolBox shared] animateIn:self.labelDescToGo];
}

//-(void) animateOut {
//    [UIView animateWithDuration:0.5
//                          delay:0
//                        options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
//                     animations:^(void)
//     {
//         [self.timeSetter setAlpha:0.0];
//     }
//                     completion:^(BOOL finished)
//     {
//         if(finished)
//         {
//         }
//     }];
//}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ( [[segue identifier] isEqualToString:@"idSegueIntroTimerExpress" ] )
    {
        IntroTimerController *controller;
        controller = (IntroTimerController*) [segue destinationViewController];
        controller.delegate = self;
        [controller setSeconds:3];
        //        controler.seconds = [NSNumber numberWithInteger:self.alarmToSet];
        //        controler.operateMode = self.addAlarmOperateMode;
    }
}

#pragma mark - Rotate

- (BOOL) shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (NSUInteger) supportedInterfaceOrientations {
    return UIInterfaceOrientationPortrait;
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if ( self.started )
        return;
    
    if ( toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft ||
        toInterfaceOrientation == UIInterfaceOrientationLandscapeRight )
    {
        [[ToolBox shared] animateOut:self.testSoundButton delay:0];
        [[self navigationController] setNavigationBarHidden:YES animated:YES];

    }
    else
    {
        [[ToolBox shared] animateIn:self.testSoundButton];
        [[self navigationController] setNavigationBarHidden:NO animated:YES];
    }
}

#pragma mark - Test
- (IBAction)testSoundBtnPress:(id)sender {
    [[AudioManager shared] playSound:eSoundMedFinish];
}

@end
