//
//  DebugTableCell.h
//  Budda Friend
//
//  Created by Przemek Urbanski on 07/01/15.
//  Copyright (c) 2015 Przemek Urbanski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DebugTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblIndex;
@property (weak, nonatomic) IBOutlet UILabel *lblMedStartDate;
@property (weak, nonatomic) IBOutlet UILabel *lblMedStartTime;
@property (weak, nonatomic) IBOutlet UILabel *lblMedTimeDone;
@property (weak, nonatomic) IBOutlet UILabel *lblTimePlanned;

@end
