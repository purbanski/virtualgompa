//
//  TableStatsViewController.h
//  Budda Friend
//
//  Created by Przemek Urbanski on 26/12/14.
//  Copyright (c) 2014 Przemek Urbanski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddRecordViewController.h"

@interface TableStatsViewController : UITableViewController<AddRecordViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tblMeditate;

@property (weak, nonatomic) IBOutlet UINavigationItem *btnAddRecord;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnSettings;


@end
