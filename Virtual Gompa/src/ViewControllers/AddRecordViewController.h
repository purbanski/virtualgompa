//
//  AddRecordViewController.h
//  Budda Friend
//
//  Created by Przemek Urbanski on 29/12/14.
//  Copyright (c) 2014 Przemek Urbanski. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddRecordViewControllerDelegate

-(void)recordSaved;

@end

@interface AddRecordViewController : UIViewController

@property (nonatomic, strong) id<AddRecordViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIDatePicker *pickerMedLen;
@property (weak, nonatomic) IBOutlet UIDatePicker *pickerMedDate;
- (IBAction)btnCancelTouch:(id)sender;

@end

//@interface AddRecordViewController : UIPageViewController
//
//@end
