//
//  FirstViewController.m
//  Budda Friend
//
//  Created by Przemek Urbanski on 24/11/14.
//  Copyright (c) 2014 Przemek Urbanski. All rights reserved.
//

#import "StatsViewController.h"
#import "StorageManager.h"
#import "ToolBox.h"

@interface StatsViewController ()

@end

@implementation StatsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [self updateStats];
}

- (void) updateStats {
    NSArray *meds = [[StorageManager sharedManager]fetchData];
    NSInteger sec;
    NSInteger total24h = 0;
    NSInteger total3d  = 0;
    NSInteger total7d  = 0;
    NSInteger total14d = 0;
    NSInteger total30d = 0;
    
    sec = [[NSDate date] timeIntervalSince1970];
    
    for (NSManagedObject *info in meds) {
        NSInteger timeStart = [[info valueForKey:@"timeStart"] integerValue];
        NSInteger timeDone = [[info valueForKey:@"timeDone"] integerValue];
        
        if ( timeStart >= [self getSecLimit:sec :1 :0 :0 :0] )
            total24h += timeDone;
        
        if ( timeStart >= [self getSecLimit:sec :3 :0 :0 :0] )
            total3d += timeDone;
        
        if ( timeStart >= [self getSecLimit:sec :7 :0 :0 :0] )
            total7d += timeDone;
        
        if ( timeStart >= [self getSecLimit:sec :14 :0 :0 :0] )
            total14d += timeDone;
        
        if ( timeStart >= [self getSecLimit:sec :30 :0 :0 :0] )
            total30d += timeDone;
        
    }
    
    [self.labelStats24Hours setText: [[ToolBox shared] secondsToTimeStr:total24h ]];
    [self.labelStats3Days setText: [[ToolBox shared] secondsToTimeStr:total3d ]];
    [self.labelStats7Days setText: [[ToolBox shared] secondsToTimeStr:total7d ]];
    [self.labelStats14Days setText: [[ToolBox shared] secondsToTimeStr:total14d ]];
    [self.labelStats30Days setText: [[ToolBox shared] secondsToTimeStr:total30d ]];

}

- (NSInteger) getSecLimit:(NSInteger)startSec
                         :(NSInteger)d
                         :(NSInteger)h
                         :(NSInteger)m
                         :(NSInteger)s
{
    NSInteger secLimit = startSec - d * 24 * 60 * 60 - h * 60 * 60 - m * 60 - s;
    return secLimit;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
