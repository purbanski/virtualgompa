
//  TableStatsViewController.m
//  Budda Friend
//
//  Created by Przemek Urbanski on 26/12/14.
//  Copyright (c) 2014 Przemek Urbanski. All rights reserved.
//

#import "TableStatsViewController.h"
#import "TestTableViewCell.h"
#import "SimpleTableCell.h"
#import "StorageManager.h"
#import "ToolBox.h"
#import "AddRecordViewController.h"
#import "AudioManager.h"

@interface TableStatsViewController ()

@property NSArray* records;

@end

@implementation TableStatsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tblMeditate.delegate = self;
    self.tblMeditate.dataSource = self;
    
    [self reload];
}

- (void)viewWillAppear:(BOOL)animated {
    [[AudioManager shared]playSound:eSoundTap];
    [[ToolBox shared]setTabBarTint:self.tabBarController];
    [self reload];
    [self.tblMeditate reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.records count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"SimpleTableCell";

    @try {
        if (![[NSBundle mainBundle] loadNibNamed:simpleTableIdentifier owner:self options:nil])
        {
            NSLog(@"Warning! Could not load myNib file.\n");
            return NO;
        }
    }
    @catch (NSException* ex)
    {
        NSLog(@"%@", [ex reason]);
    }
    SimpleTableCell *cell;
    cell = (SimpleTableCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib;
        nib = [[NSBundle mainBundle] loadNibNamed:simpleTableIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    if ( ! [self.records count] )
        return cell;
    
    NSInteger ind = indexPath.row;
    NSManagedObject *r;

    r = [self.records objectAtIndex:ind] ;
    
    NSInteger timeStart     = [[r valueForKey:@"timeStart"] integerValue];
    NSInteger timeDone      = [[r valueForKey:@"timeDone"] integerValue];
//    NSInteger timePlanned   = [[r valueForKey:@"timePlanned"] integerValue];

    cell.dateStartLabel.text    = [ NSString stringWithFormat:@"%@",
                                   [[ToolBox shared] secondsToDateStr:timeStart ]];
    NSString *tmStart;
    tmStart = [ NSString stringWithFormat:@"%@", [[ToolBox shared] secondsToTimeStr:timeStart]];
    tmStart = [tmStart substringWithRange:NSMakeRange(0, tmStart.length-3)];
    cell.timeStartLabel.text     = tmStart;
    
    NSString *tmDone;
    tmDone = [ NSString stringWithFormat:@"%@", [[ToolBox shared] secondsToTimeStrShort:timeDone]];
//    tmDone = [tmDone substringWithRange:NSMakeRange(0, tmDone.length-3)];
    cell.timeDoneLabel.text     = tmDone;

    cell.labelIndex.text = [NSString stringWithFormat:@"%d", indexPath.row+1];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0;
}

-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return nil;
//    NSArray *ret = @[@"A",@"B",@"E",
//                     @"F",@"G",@"H",
//                     @"I",@"J",@"K",
//                     @"L",@"M",@"N"];
//    return ret;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
//    return NO;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [[StorageManager sharedManager] deleteRecord:indexPath.row];
        [self reload];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ( [[segue identifier] isEqualToString:@"idSegueAddRecord" ] )
    {
        AddRecordViewController *controler = [segue destinationViewController];
        controler.delegate = self;
    }

    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

#pragma mark - AddRecordControllerDelegate
-(void)recordSaved
{
    [self reload];
}
#pragma mark - Private
-(void) reload
{
    self.records = [[StorageManager sharedManager]fetchData];
}

@end
