//
//  TestTableViewCell.m
//  Budda Friend
//
//  Created by Przemek Urbanski on 27/12/14.
//  Copyright (c) 2014 Przemek Urbanski. All rights reserved.
//

#import "TestTableViewCell.h"

@implementation TestTableViewCell

+(id)create:(UITableView*)tableView indexPath:(NSIndexPath *)indexPath
{
    TestTableViewCell  *cell;
    NSString *text, *dtext;
    NSString *identifier;
    identifier = @"StatsCell";

     
    cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[TestTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    text = [NSString stringWithFormat:@"%@", @"test" ];
    cell.textLabel.text = text;
    
    dtext = [NSString stringWithFormat:@"%@ \t\t %@", @"qwe", @"2342"];
    cell.detailTextLabel.text = dtext;
    
    cell.textLabel.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
    cell.detailTextLabel.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
    
    UILabel *l1  = (UILabel *)[cell viewWithTag:100];
    UILabel *l2  = (UILabel *)[cell viewWithTag:101];
    [l1 setText:@"dupa"];
    [l2 setText:@"dupa"];
    
    //  [cell addSubview:l];
    //
    //    // Configure the cell...
    //
    return cell;
    
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
