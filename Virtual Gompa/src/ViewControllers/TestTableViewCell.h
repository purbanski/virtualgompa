//
//  TestTableViewCell.h
//  Budda Friend
//
//  Created by Przemek Urbanski on 27/12/14.
//  Copyright (c) 2014 Przemek Urbanski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestTableViewCell : UITableViewCell

+(id) create:(UITableView*)tableView indexPath:(NSIndexPath*)indexPath;
@end
