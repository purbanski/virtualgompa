//
//  SimpleTableCell.m
//  Budda Friend
//
//  Created by Przemek Urbanski on 27/12/14.
//  Copyright (c) 2014 Przemek Urbanski. All rights reserved.
//

#import "SimpleTableCell.h"

@implementation SimpleTableCell

@synthesize timeDoneLabel   = _timeDoneLabel;
@synthesize timeStartLabel  = _timeStartLabel;
@synthesize dateStartLabel  = _dateStartLabel;
//@synthesize thumbnailImageView = _thumbnailImageView;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
