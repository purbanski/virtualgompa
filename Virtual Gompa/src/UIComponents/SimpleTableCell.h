//
//  SimpleTableCell.h
//  Budda Friend
//
//  Created by Przemek Urbanski on 27/12/14.
//  Copyright (c) 2014 Przemek Urbanski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SimpleTableCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *timeStartLabel;
@property (nonatomic, weak) IBOutlet UILabel *dateStartLabel;
@property (nonatomic, weak) IBOutlet UILabel *timeDoneLabel;
//@property (nonatomic, weak) IBOutlet UIImageView *thumbnailImageView;
@property (weak, nonatomic) IBOutlet UILabel *labelIndex;

@end
