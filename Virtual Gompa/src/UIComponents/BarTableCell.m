//
//  BarTableCell.m
//  Budda Friend
//
//  Created by Przemek Urbanski on 31/12/14.
//  Copyright (c) 2014 Przemek Urbanski. All rights reserved.
//

#import "BarTableCell.h"
#import "BarTableController.h"

@interface BarTableCell ()


@end

@implementation BarTableCell

@synthesize labelDate = _labelDate;
@synthesize labelTimeDone = _labelTimeDone;
@synthesize labelTimeDoneMark = _labelTimeDoneMark;
@synthesize labelIndex = _labelIndex;
@synthesize labelDebug = _labelDebug;

@synthesize timeDone = _timeDone;
@synthesize maxTimeDone = _maxTimeDone;
@synthesize index = _index;

-(id) init {
    if( [super init] )
    {
        self.timeDone = 0;
    };
    return self;
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];
//    BarTableController *vc = (BarTableController*) self.window.rootViewController;
//    [vc.tblMeditate reloadData];
    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];

    float xstart = 125.0f;
    float xdelta = 5.0f;
    float xscale;
    
    if (self.maxTimeDone > 3 * 60 * 60 )
    {
        xscale = 3.0f*60.0f*60.0f / (float)self.maxTimeDone ;
    }
    else
        xscale = 1.0f;
    self.labelTimeDoneMark.text = @"";
    self.labelIndex.text = [NSString stringWithFormat:@"%d", self.index ];
    self.labelTimeDoneMark.frame = CGRectMake(xstart, 0, self.timeDone/60.0f * xscale, self.frame.size.height);
    self.labelTimeDone.frame = CGRectMake(xstart+xdelta, 0, self.frame.size.width, self.frame.size.height);
    
    UIColor *bgColor;
    
    float t = self.timeDone;
    float mt = self.maxTimeDone;
    float sat = 0.55;
//    NSLog(@"%f %f", t, mt);
    
    sat *= t/mt;
    
    bgColor = [UIColor colorWithHue:0.25 saturation:0.55 brightness:0.97 alpha:100];
    self.labelTimeDoneMark.backgroundColor = bgColor;
    
    [self.labelTimeDoneMark setHidden:FALSE];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
        self.labelTimeDoneMark.text = @"reuse";
    self.labelDebug.text = @"reuse";
    self.labelTimeDoneMark.frame = CGRectMake(0, 0, 0.0f, 0.0f);
    [self.labelTimeDoneMark setHidden:TRUE];
//    [self setEditing:NO animated:NO];
//    [self setNeedsLayout];
//    [self layoutSubviews];
//    [self test];
}

@end
