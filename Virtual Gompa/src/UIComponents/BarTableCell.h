//
//  BarTableCell.h
//  Budda Friend
//
//  Created by Przemek Urbanski on 31/12/14.
//  Copyright (c) 2014 Przemek Urbanski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BarTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelDate;
@property (weak, nonatomic) IBOutlet UILabel *labelTimeDone;
@property (weak, nonatomic) IBOutlet UILabel *labelTimeDoneMark;
@property (weak, nonatomic) IBOutlet UILabel *labelIndex;
@property (weak, nonatomic) IBOutlet UILabel *labelToday;
@property (weak, nonatomic) IBOutlet UILabel *labelDayOfWeek;
@property (weak, nonatomic) IBOutlet UILabel *labelDebug;

@property NSInteger timeDone;
@property NSInteger maxTimeDone;
@property NSInteger index;

@end
