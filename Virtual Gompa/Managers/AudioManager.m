//
//  AudioManager.m
//  Budda Friend
//
//  Created by Przemek Urbanski on 01/12/14.
//  Copyright (c) 2014 Przemek Urbanski. All rights reserved.
//

#import "AudioManager.h"

@implementation AudioManager

@synthesize soundTapURL;
@synthesize soundTap;

@synthesize soundMeditateFinishURL;
@synthesize soundMeditateFinish;

static AudioManager *sharedAudioManager = nil;

+ (id)shared {
    if (sharedAudioManager == nil ) {
        sharedAudioManager = [[ super allocWithZone:NULL] init];
    }
    return sharedAudioManager;
}

- (id)init {
    if (self = [super init]) {
        NSURL *sound   = [[NSBundle mainBundle] URLForResource: @"tap"
                                                    withExtension: @"aif"];
        
        self.soundTapURL = (__bridge CFURLRef)sound ;
        AudioServicesCreateSystemSoundID (soundTapURL, &soundTap);
        
        sound   = [[NSBundle mainBundle] URLForResource: @"medFinish"
                                          withExtension: @"aif"];
        
        self.soundMeditateFinishURL = (__bridge CFURLRef)sound ;
        AudioServicesCreateSystemSoundID (soundMeditateFinishURL, &soundMeditateFinish);
    }
    return self;
}

-(void)dealloc {
}

#pragma mark sounds
- (void) playSoundTap {
    AudioServicesPlaySystemSound (soundTap);
}


- (void) playSoundMedFinish {
    AudioServicesPlayAlertSound (soundMeditateFinish);
}



@end
